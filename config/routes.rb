Rails.application.routes.draw do
  root 'test#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get "/wound", to: "wound#index"
  post "/wound/create_wound", to: "wound#create_wound"
  get "/wound/show_examination", to: "wound#show_examination"
  get "/wound/create_examination", to: "wound#create_examination"
  get "/wound/show_wounddoc_tab", to: "wound#show_wounddoc_tab"
  get "/wound/show_create_documentation", to: "wound#show_create_documentation"
  post "/wound/create_documentation", to: "wound#create_documentation"
  get "/wound/show_documentation", to: "wound#show_documentation"
  patch "/wound/update_documentaion", to: "wound#update_documentation"
  patch "/wound/update_examination", to: "wound#update_examination"
  post "/reminders/update_reminder", to: "reminders#update_reminder"
  get "/reminders/get_reminders",to: "reminders#get_reminders"
  get "/reminders/get_all_reminders_from_patient",to: "reminders#get_all_reminders_from_patient"
  get "/reminders/get_all_reminders",to: "reminders#get_all_reminders"
end
