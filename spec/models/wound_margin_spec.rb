require 'rails_helper'

RSpec.describe WoundMargin, type: :model do
  it "ensure presence" do
    woundmargin = WoundMargin.new().save
    expect(woundmargin).to eq true
  end
   
  it " without parameter should be valid" do
    woundmargin = WoundMargin.new()
    expect(woundmargin).to be_valid
  end

  it "should save successfully" do
    woundmargin = WoundMargin.new(description:"MARGIN").save
    expect(woundmargin).to eq true
  end

  it " with parameter should be valid" do
    woundmargin = WoundMargin.new(description:"MARGIN")
    expect(woundmargin.description).to eq('MARGIN')
  end

end