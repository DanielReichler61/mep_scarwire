require 'rails_helper'

RSpec.describe WounddocPicture, type: :model do
  it "ensure presence" do
    wounddocpicture = WounddocPicture.new().save
    expect(wounddocpicture).to eq false
  end
   
  it " without parameter should not be valid" do
    wounddocpicture = WounddocPicture.new()
    expect(wounddocpicture).to_not be_valid
  end

  it "should save successfully" do
    wounddocpicture = WounddocPicture.new(PATH:"PICS").save
    expect(wounddocpicture).to eq false
  end

  it " with parameter should be valid" do
    wounddocpicture = WounddocPicture.new(PATH:"PICS")
    expect(wounddocpicture.PATH).to eq('PICS')
  end

end