require 'rails_helper'

RSpec.describe Factor, type: :model do
  it "ensure presence" do
    factor = Factor.new().save
    expect(factor).to eq true
  end
   
  it "factor without parameter should be valid" do
    factor = Factor.new()
    expect(factor).to be_valid
  end

  it "should save successfully" do
    factor = Factor.new(factor_name:"Name").save
    expect(factor).to eq true
  end

  it " with parameter should be valid" do
    factor = Factor.new(factor_name:"Name")
    expect(factor.factor_name).to eq('Name')
  end

end
