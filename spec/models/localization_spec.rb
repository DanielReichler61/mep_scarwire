require 'rails_helper'

RSpec.describe Localization, type: :model do
  it "ensure presence" do
    localization = Localization.new().save
    expect(localization).to eq false
  end
   
  it " without parameter should be not valid" do
    localization = Localization.new()
    expect(localization).to_not be_valid
  end

  it "should save successfully" do
    localization = Localization.new(recidivism:5).save
    expect(localization).to eq false
  end

  # it "with parameter should be valid" do
  #   localization = Localization.new(recidivism:5, body_part:20).save
  #   expect(localization.recidivism).to eq(5)
  #   expect(localization.body_part).to eq(20)
  # end


end