require 'rails_helper'

RSpec.describe PhaseOption, type: :model do
  it "ensure presence" do
    phaseoption = PhaseOption.new().save
    expect(phaseoption).to eq false
  end
   
  it " without parameter should be valid" do
    phaseoption = PhaseOption.new()
    expect(phaseoption).to_not be_valid
  end

  it "should save successfully" do
    phaseoption = PhaseOption.new(value:"OPTION").save
    expect(phaseoption).to eq false
  end

  it " with parameter should be valid" do
    phaseoption = PhaseOption.new(value:"OPTION")
    expect(phaseoption.value).to eq('OPTION')
  end

end