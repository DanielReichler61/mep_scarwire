require 'rails_helper'

RSpec.describe BatchDocumentation, type: :model do

  it " without parameter should not be valid" do
    batchdocumentation = BatchDocumentation.new()
    expect(batchdocumentation).to_not be_valid
  end

  # it " with parameter should be valid" do
  #   batchdocumentation = Cave.new(batch_number:123, medical_product:"Pinzette", documentation_id:546)
  #   expect(batchdocumentation.batch_number).to eq(123)
  #   expect(batchdocumentation.medical_product).to eq("Pinzette")
  #   expect(batchdocumentation.documentation_id).to eq(546)
  # end

  it "should save successfully" do
    batchdocumentation = BatchDocumentation.new(batch_number: 123, medical_product: "Pinzette", documentation_id:'546').save
    expect(batchdocumentation).to eq false
  end
end
