require 'rails_helper'

RSpec.describe Wound, type: :model do
    it "ensure presence" do
      wound = Wound.new().save
      expect(wound).to eq false
    end
     
    it " without parameter should not be valid" do
      wound = Wound.new()
      expect(wound).to_not be_valid
    end
  
    it "should save successfully" do
      wound = Wound.new(designation:"WOUND").save
      expect(wound).to eq false
    end
  
    it " with parameter should be valid" do
      wound = Wound.new(designation:"WOUND")
      expect(wound.designation).to eq('WOUND')
    end
  
  end