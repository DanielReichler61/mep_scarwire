require 'rails_helper'

RSpec.describe SkinCare, type: :model do
  it "ensure presence" do
    skincare = SkinCare.new().save
    expect(skincare).to eq true
  end
   
  it " without parameter should be valid" do
    skincare = SkinCare.new()
    expect(skincare).to be_valid
  end

  it "should save successfully" do
    skincare = SkinCare.new(description:"SKINCARE").save
    expect(skincare).to eq true
  end

  it " with parameter should be valid" do
    skincare = SkinCare.new(description:"SKINCARE")
    expect(skincare.description).to eq('SKINCARE')
  end

end