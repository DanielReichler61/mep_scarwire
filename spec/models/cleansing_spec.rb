require 'rails_helper'

RSpec.describe Cleansing, type: :model do
  it "Cleansing without parameter should be valid" do
    cleansing = Cleansing.new()
    expect(cleansing).to be_valid
  end

  it "cleansing with parameter should be valid" do
    cleansing = Cleansing.new(data_type_id:1234)
    expect(cleansing.data_type_id).to eq(1234)
  end

  # it "should save successfully" do
  #   cleansing = Cleansing.new(description:'Hautpflege',data_type_id:1234).save
  #   expect(cleansing).to eq false
  # end

end
