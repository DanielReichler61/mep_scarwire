require 'rails_helper'


RSpec.describe Infection, type: :model do
  it "ensure presence" do
    infection = Infection.new().save
    expect(infection).to eq true
  end
   
  it " without parameter should be valid" do
    infection = Infection.new()
    expect(infection).to be_valid
  end

  it "should save successfully" do
    infection = Infection.new(description:"INFECT").save
    expect(infection).to eq true
  end

  it " with parameter should be valid" do
    infection = Infection.new(description:"INFECT")
    expect(infection.description).to eq('INFECT')
  end

end