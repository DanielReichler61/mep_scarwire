require 'rails_helper'

RSpec.describe DocumentationsInfection, type: :model do
  it "should be valid" do
    documentationinfection = DocumentationsInfection.new(value:"Infection")
    expect(documentationinfection.value).to eq("Infection")
  end

  it " without parameter should not be valid" do
    documentationinfection = DocumentationsInfection.new()
    expect(documentationinfection).to_not be_valid
  end

  it "ensure presence" do
    documentationinfection = DocumentationsInfection.new().save
    expect(documentationinfection).to eq false
  end

  it "should save successfully" do
    documentationinfection = DocumentationsInfection.new(value:"Infection").save
    expect(documentationinfection).to eq false
  end

end