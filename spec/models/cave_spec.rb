require 'rails_helper'

RSpec.describe Cave, type: :model do
  it "cave without parameter should not be valid" do
      cave = Cave.new()
      expect(cave).to_not be_valid
  end

  it "cave with parameter should be valid" do
    cave = Cave.new(allergy:'Pollen',resistant_germs:'Keime',other:'schwerhörig')
    expect(cave.allergy).to eq('Pollen')
    expect(cave.resistant_germs).to eq('Keime')
    expect(cave.other).to eq('schwerhörig')
  end

  it "should save successfully" do
    cave = Cave.new(allergy: "Pollen", resistant_germs: "Keime", other:'schwerhörig').save
    expect(cave).to eq false
  end

end

