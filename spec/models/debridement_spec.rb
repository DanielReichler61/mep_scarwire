require "rails_helper"

RSpec.describe Debridement, type: :model do

    
    it "ensure kind presence" do
      debridement = Debridement.new().save
      expect(debridement).to eq true
    end
     
    it "debridement without parameter should be valid" do
      debridement = Debridement.new()
      expect(debridement).to be_valid
  end

    it "should save successfully" do
      debridement = Debridement.new(description: "Seriös").save
      expect(debridement).to eq true

    end

    it " with parameter should be valid" do
      debridement = Debridement.new(description:'DEBRIDEMENT')
      expect(debridement.description).to eq('DEBRIDEMENT')

    end
  
end
