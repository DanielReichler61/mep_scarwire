require 'rails_helper'

RSpec.describe SkinCondition, type: :model do
  it "ensure presence" do
    skincondition = SkinCondition.new().save
    expect(skincondition).to eq true
  end
   
  it " without parameter should be valid" do
    skincondition = SkinCondition.new()
    expect(skincondition).to be_valid
  end

  it "should save successfully" do
    skincondition = SkinCondition.new(description:"CONDITION").save
    expect(skincondition).to eq true
  end

  it " with parameter should be valid" do
    skincondition = SkinCondition.new(description:"CONDITION")
    expect(skincondition.description).to eq('CONDITION')
  end

end