require 'rails_helper'

RSpec.describe Patient, type: :model do
    it "employee firstname should be valid" do
      patient = Patient.new(first_name:'Peter',last_name:'Durstig',birth_date:'05.10.1937')
      expect(patient.first_name).to eq('Peter')
      expect(patient.last_name).to eq('Durstig')
      expect(patient.birth_date).to eql(Date.parse('05.10.1937'))
    end

    it "employee without parameter should be valid" do
      patient = Patient.new(first_name:'Hans')
      expect(patient.first_name).to eq('Hans')
    end

    it "ensure presence" do
      patient = Patient.new().save
      expect(patient).to eq true
    end
     
    it " without parameter should be valid" do
      patient = Patient.new()
      expect(patient).to be_valid
    end
  
    it "should save successfully" do
      patient = Patient.new(first_name:"Peter").save
      expect(patient).to eq true
    end

end
