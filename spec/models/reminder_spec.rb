require 'rails_helper'

RSpec.describe Reminder, type: :model do
  it "ensure presence" do
    reminder = Reminder.new().save
    expect(reminder).to eq true
  end
   
  it " without parameter should be valid" do
    reminder = Reminder.new()
    expect(reminder).to be_valid
  end

  it "should save successfully" do
    reminder = Reminder.new(text:"Eine Erinnerung").save
    expect(reminder).to eq true
  end

  it " with parameter should be valid" do
    reminder = Reminder.new(text:"Eine Erinnerung")
    expect(reminder.text).to eq('Eine Erinnerung')
  end

end