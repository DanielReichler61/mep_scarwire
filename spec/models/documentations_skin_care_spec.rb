require 'rails_helper'

RSpec.describe DocumentationsSkinCare, type: :model do
  it "should be valid" do
    documentationskincare = DocumentationsSkinCare.new(value:"SkinCare")
    expect(documentationskincare.value).to eq("SkinCare")
  end

  it " without parameter should not be valid" do
    documentationskincare = DocumentationsSkinCare.new()
    expect(documentationskincare).to_not be_valid
  end

  it "ensure presence" do
    documentationskincare = DocumentationsSkinCare.new().save
    expect(documentationskincare).to eq false
  end

  it "should save successfully" do
    documentationskincare = DocumentationsSkinCare.new(value:"SkinCare").save
    expect(documentationskincare).to eq false
  end
end