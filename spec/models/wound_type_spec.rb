require 'rails_helper'

RSpec.describe WoundType, type: :model do
  it "WoundType name should be valid" do
    woundType = WoundType.new(type_name:'Dekubitus Grad 3')
    expect(woundType.type_name).to eq('Dekubitus Grad 3')
  end

  it "woundtype without parameter should be valid" do
    woundType = WoundType.new()
    expect(woundType).to be_valid
  end
  
end
