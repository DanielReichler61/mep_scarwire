require 'rails_helper'

RSpec.describe WoundPhase, type: :model do
    it "ensure presence" do
      woundphase = WoundPhase.new().save
      expect(woundphase).to eq true
    end
     
    it " without parameter should be valid" do
      woundphase = WoundPhase.new()
      expect(woundphase).to be_valid
    end
  
    it "should save successfully" do
      woundphase = WoundPhase.new(phase:"PHASE").save
      expect(woundphase).to eq true
    end
  
    it " with parameter should be valid" do
      woundphase = WoundPhase.new(phase:"PHASE")
      expect(woundphase.phase).to eq('PHASE')
    end
  
  end