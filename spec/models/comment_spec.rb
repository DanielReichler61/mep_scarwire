require 'rails_helper'

RSpec.describe Comment, type: :model do
  it "Comment without parameter should  be valid" do
    comment = Comment.new()
    expect(comment).to_not be_valid
  end

  # it "comment with parameter should be valid" do
  #   comment = Comment.new(content:"Kommentar",documentation_id:123)
  #   expect(comment.content).to eq("Kommentar")
  #   expect(comment.documentation_id).to eq(123)
  # end

  it "should save successfully" do
    comment = Comment.new(content:'Kommentar').save
    expect(comment).to eq false
  end
end
