require 'rails_helper'

RSpec.describe Vorlage, type: :model do
  it "ensure presence" do
    vorlage = Vorlage.new().save
    expect(vorlage).to eq true
  end
   
  it " without parameter should be valid" do
    vorlage = Vorlage.new()
    expect(vorlage).to be_valid
  end

  it "should save successfully" do
    vorlage = Vorlage.new(template_content:"Meine Vorlage").save
    expect(vorlage).to eq true
  end

  it " with parameter should be valid" do
    vorlage = Vorlage.new(template_content:"Meine Vorlage")
    expect(vorlage.template_content).to eq('Meine Vorlage')
  end

end