require 'rails_helper'

RSpec.describe FactorsWound, type: :model do
  it "ensure presence" do
    factorswound = FactorsWound.new().save
    expect(factorswound).to eq false
  end
   
  it "factorswound without parameter should not
   be valid" do
    factorswound = FactorsWound.new()
    expect(factorswound).to_not be_valid
  end

  it "should save successfully" do
    factorswound = FactorsWound.new(value:"FACTOR").save
    expect(factorswound).to eq false
  end

  it " with parameter should be valid" do
    factorswound = FactorsWound.new(value:"FACTOR")
    expect(factorswound.value).to eq('FACTOR')
  end

end
