require 'rails_helper'

RSpec.describe WoundPhaseOption, type: :model do
    it "ensure presence" do
      woundphaseoption = WoundPhaseOption.new().save
      expect(woundphaseoption).to eq false
    end
     
    it " without parameter should not be valid" do
      woundphaseoption = WoundPhaseOption.new()
      expect(woundphaseoption).to_not be_valid
    end
  
    it "should save successfully" do
      woundphaseoption = WoundPhaseOption.new(description:"OPTIONS").save
      expect(woundphaseoption).to eq false
    end
  
    it " with parameter should be valid" do
      woundphaseoption = WoundPhaseOption.new(description:"OPTIONS")
      expect(woundphaseoption.description).to eq('OPTIONS')
    end
  
  end