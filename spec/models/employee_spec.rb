require 'rails_helper'

RSpec.describe Employee, type: :model do
  it "employee firstname should be valid" do
    employee = Employee.new(first_name:'abc',last_name:'James',doctor:true)
    expect(employee.first_name).to eq('abc')
    expect(employee.last_name).to eq('James')
    expect(employee.doctor).to eq(true)
  end

  it "employee without parameter should be valid" do
    employee = Employee.new()
    expect(employee).to be_valid

  end
  
end

