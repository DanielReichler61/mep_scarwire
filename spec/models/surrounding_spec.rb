require 'rails_helper'

RSpec.describe Surrounding, type: :model do
  it "ensure presence" do
    surrounding = Surrounding.new().save
    expect(surrounding).to eq true
  end
   
  it " without parameter should be valid" do
    surrounding = Infection.new()
    expect(surrounding).to be_valid
  end

  it "should save successfully" do
    surrounding = Infection.new(description:"SURROUNDING").save
    expect(surrounding).to eq true
  end

  it " with parameter should be valid" do
    surrounding = Infection.new(description:"SURROUNDING")
    expect(surrounding.description).to eq('SURROUNDING')
  end

end