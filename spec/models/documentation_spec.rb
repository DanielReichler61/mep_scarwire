require 'rails_helper'

RSpec.describe Documentation, type: :model do
  it "should be valid" do
    documentation = Documentation.new(length:1,width:2,depth:3)
    expect(documentation.length).to eql(1)
    expect(documentation.width).to eql(2)
    expect(documentation.depth).to eql(3)
  end

  it " without parameter should not be valid" do
    documentation = Documentation.new()
    expect(documentation).to_not be_valid
  end

  it "ensure presence" do
    documentation = Documentation.new().save
    expect(documentation).to eq false
  end

  it "should save successfully" do
    documentation = Documentation.new(length: 1).save
    expect(documentation).to eq false
  end

end