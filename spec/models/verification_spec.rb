require 'rails_helper'

RSpec.describe Verification, type: :model do
  it "ensure presence" do
    verification = Verification.new().save
    expect(verification).to eq false
  end
   
  it " without parameter should not be valid" do
    verification = Verification.new()
    expect(verification).to_not be_valid
  end

  it "should save successfully" do
    verification = Verification.new(field:"VERIFICATION").save
    expect(verification).to eq false
  end

  it " with parameter should be valid" do
    verification = Verification.new(field:"VERIFICATION")
    expect(verification.field).to eq('VERIFICATION')
  end

end