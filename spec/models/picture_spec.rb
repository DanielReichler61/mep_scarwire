require 'rails_helper'

RSpec.describe Picture, type: :model do
  it "ensure presence" do
    picture = Picture.new().save
    expect(picture).to eq false
  end
   
  it " without parameter should not be valid" do
    picture = Picture.new()
    expect(picture).to_not be_valid
  end

  it " with parameter should be valid" do
    picture = Picture.new(PATH:'Wound/PICS')
    expect(picture.PATH).to eq('Wound/PICS')
  end

end
