require 'rails_helper'

RSpec.describe Post, type: :model do
  it "ensure presence" do
    post = Post.new().save
    expect(post).to eq true
  end
   
  it " without parameter should be valid" do
    post = Post.new()
    expect(post).to be_valid
  end

  it "should save successfully" do
    post = Post.new(author:"Kevin James").save
    expect(post).to eq true
  end

  it " with parameter should be valid" do
    post = Post.new(author:'Kevin James')
    expect(post.author).to eq('Kevin James')
  end

end