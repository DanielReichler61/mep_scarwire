require 'rails_helper'

RSpec.describe Dressing, type: :model do
      
  it "ensure presence" do
    dressing = Dressing.new().save
    expect(dressing).to eq true
  end
   
  it "dressing without parameter should be valid" do
    dressing = Dressing.new()
    expect(dressing).to be_valid
end

  it "should save successfully" do
    dressing = Dressing.new(description:"DRESSING").save
    expect(dressing).to eq true

  end

  it " with parameter should be valid" do
    dressing = Dressing.new(description:"DRESSING")
    expect(dressing.description).to eq('DRESSING')

  end

end
