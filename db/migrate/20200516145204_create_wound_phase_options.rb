class CreateWoundPhaseOptions < ActiveRecord::Migration[6.0]
  def change
    create_table :wound_phase_options do |t|
      t.string :description
      t.references :wound_phase , foreign_key: true
      t.references :data_type , foreign_key: true
      t.timestamps
    end
    create_table :phase_options, id:false do |t|
      t.references :documentation,foreign_key: true 
      t.references :wound_phase_option, foreign_key: true
      t.string :value 
    end 
  end
end
