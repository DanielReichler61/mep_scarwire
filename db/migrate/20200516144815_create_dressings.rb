class CreateDressings < ActiveRecord::Migration[6.0]
  def change
    create_table :dressings do |t|
      t.string :description
      t.references :data_type , foreign_key: true
      t.timestamps
    end
    create_table :documentations_dressings, id:false  do |t|
      t.references :documentation, foreign_key: true 
      t.references :dressing, foreign_key: true 
      t.string :value
    end 
  end
end
