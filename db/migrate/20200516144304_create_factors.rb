class CreateFactors < ActiveRecord::Migration[6.0]
  def change
    create_table :factors do |t|
      t.string :factor_name
      t.references :data_type , foreign_key: true
      t.timestamps
    end
    create_table :factors_wounds, id:false do |t| 
      t.references :wound , foreign_key: true 
      t.references :factor , foreign_key: true 
      t.string :value
    end 
  end
end
