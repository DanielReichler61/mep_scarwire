class CreateWounds < ActiveRecord::Migration[6.0]
  def change
    create_table :wounds do |t|
      t.boolean :permission
      t.text :comment
      t.string :designation
      t.string :images
      t.references :patient , foreign_key: true
      t.references :wound_type , foreign_key: true 
      t.timestamps
    end
  end
end
