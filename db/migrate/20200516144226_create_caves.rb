class CreateCaves < ActiveRecord::Migration[6.0]
  def change
    create_table :caves,id: false do |t|
      t.string :allergy
      t.string :resistant_germs
      t.string :other
      t.references :patient , foreign_key: true , null: false, primary_key:true
      t.timestamps
    end
  end
end
