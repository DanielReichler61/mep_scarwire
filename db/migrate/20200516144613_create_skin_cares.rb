class CreateSkinCares < ActiveRecord::Migration[6.0]
  def change
    create_table :skin_cares do |t|
      t.string :description
      t.references :data_types , foreign_key: true
      t.timestamps
    end
    create_table :documentations_skin_cares, id: false do |t|
      t.references :documentation , foreign_key: true 
      t.references :skin_care , foreign_key: true
      t.string :value 
    end
  end
end
