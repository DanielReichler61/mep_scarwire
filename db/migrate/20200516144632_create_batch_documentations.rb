class CreateBatchDocumentations < ActiveRecord::Migration[6.0]
  def change
    create_table :batch_documentations do |t|
      t.string :batch_number
      t.string :medical_product
      t.references :documentation , foreign_key: true 
      t.timestamps
    end
  end
end
