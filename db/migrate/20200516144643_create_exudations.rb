class CreateExudations < ActiveRecord::Migration[6.0]
  def change
    create_table :exudations do |t|
      t.string :description
      t.references :data_type ,foreign_key: true
      t.timestamps
    end
    create_table :documentations_exudations,id: false  do |t| 
      t.references :documentation , foreign_key: true  
      t.references :exudation , foreign_key: true 
    end
  end
end
