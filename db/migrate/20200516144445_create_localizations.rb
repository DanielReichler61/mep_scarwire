class CreateLocalizations < ActiveRecord::Migration[6.0]
  def change
    create_table :localizations, id: false do |t|
      t.references :wound, foreign_key: true , null: false , primary_key: true 
      t.date :wound_since 
      t.integer :recidivism
      t.integer :body_part
      t.timestamps
    end
  end
end
