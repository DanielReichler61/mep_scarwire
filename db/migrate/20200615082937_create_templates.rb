class CreateTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :vorlages do |t|
      t.string :template_name
      t.text :template_content

      t.timestamps
    end
  end
end
