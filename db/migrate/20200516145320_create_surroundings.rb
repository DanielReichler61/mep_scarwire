class CreateSurroundings < ActiveRecord::Migration[6.0]
  def change
    create_table :surroundings do |t|
      t.string :description 
      t.references :data_type , foreign_key: true
      t.timestamps
    end
    create_table :documentations_surroundings, id: false do |t|
      t.references :documentation, foreign_key: true 
      t.references :surrounding, foreign_key: true 
    
    end 
  end
end
