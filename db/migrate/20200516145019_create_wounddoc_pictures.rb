class CreateWounddocPictures < ActiveRecord::Migration[6.0]
  def change
    create_table :wounddoc_pictures do |t|
      t.references :documentation ,foreign_key: true
      t.string :PATH
      t.timestamps
    end
  end
end
