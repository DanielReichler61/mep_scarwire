class CreateReminders < ActiveRecord::Migration[6.0]
  def change
    create_table :reminders do |t|
      t.string :text
      t.datetime :publication_date
      t.boolean :checked , :default => false 
      t.references :patient, foreign_key:true
      t.references :documentation , foreign_key:true
      t.string :justification_text
      t.integer :counter , :default => 0
      t.timestamps
    end
  end
end
