class CreateCleansings < ActiveRecord::Migration[6.0]
  def change
    create_table :cleansings do |t|
      t.string :description
      t.references :data_type , foreign_key: true
      t.timestamps
    end
    create_table :cleansings_documentations, id: false  do |t|
      t.references :cleansing , foreign_key: true 
      t.references :documentation, foreign_key: true 
    end 
  end
end
