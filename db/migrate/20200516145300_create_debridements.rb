class CreateDebridements < ActiveRecord::Migration[6.0]
  def change
    create_table :debridements do |t|
      t.string :description
      t.references :data_type , foreign_key: true
      t.timestamps
    end
    create_table :debridements_documentations, id: false  do |t|
      t.references :documentation, foreign_key: true
      t.references :debridement, foreign_key: true
    end 
  end
end
