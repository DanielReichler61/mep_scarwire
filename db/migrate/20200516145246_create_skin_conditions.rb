class CreateSkinConditions < ActiveRecord::Migration[6.0]
  def change
    create_table :skin_conditions do |t|
      t.string :description 
      t.references :data_type , foreign_key: true
      t.timestamps
    end
    create_table :documentations_skin_conditions, id:false  do |t|
      t.references :documentation , foreign_key: true 
      t.references :skin_condition , foreign_key: true 
    end 
  end
end
