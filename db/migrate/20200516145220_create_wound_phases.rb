class CreateWoundPhases < ActiveRecord::Migration[6.0]
  def change
    create_table :wound_phases do |t|
      t.string :phase
      t.timestamps
    end
  end
end
