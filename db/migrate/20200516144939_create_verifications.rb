class CreateVerifications < ActiveRecord::Migration[6.0]
  def change
    create_table :verifications do |t|
      t.string :field 
      t.references :employee , foreign_key: true 
      t.references :documentation , foreign_key: true
      t.timestamps
    end
  end
end
