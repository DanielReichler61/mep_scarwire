class CreateDocumentations < ActiveRecord::Migration[6.0]
  def change
    create_table :documentations do |t|
      t.decimal :width
      t.decimal :depth
      t.decimal :length
      t.boolean :bandage_change
      t.integer :pain
      t.boolean :visit_doctor
      t.boolean :permission
      t.string :images
      t.references :wound ,foreign_key: true 
      t.references :employee , foreign_key: true 
      t.timestamps
    end
  end
end
