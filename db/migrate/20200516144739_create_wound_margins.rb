class CreateWoundMargins < ActiveRecord::Migration[6.0]
  def change
    create_table :wound_margins do |t|
      t.string :description
      t.timestamps
    end
    create_table :documentations_wound_margins,id: false  do |t|
      t.references :documentation ,foreign_key: true 
      t.references :wound_margin , foreign_key: true
    end 
  end
end
