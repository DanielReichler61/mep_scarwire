# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#

DataType.create([{data_type: "St"},{data_type: "Int"},{data_type: "Date"},{data_type: "Radio"},{data_type: "Checkbox"}])
Patient.create([{first_name: "Hans" , last_name: "Peter", birth_date: "27.10.1937"},
    {first_name: "Peter" , last_name: "Durstig", birth_date: "05.10.1937"},
    {first_name: "Klaus" , last_name: "Müller", birth_date: "27.01.1950"},
    {first_name: "Laura" , last_name: "Fischer", birth_date: "14.07.1955"}
])

Employee.create([{first_name: "Kevin" ,last_name: "James", doctor: true},
    {first_name: "Klara" ,last_name: "Müller", doctor: false},
    {first_name: "Max" ,last_name: "Meier", doctor: true},
    {first_name: "Mark" ,last_name: "Bauer", doctor: false}  
])
WoundType.create([{type_name: "Ulcus cruris venosum"},
    {type_name:  "Ulcus cruris arteriosum"},
    {type_name: "Ulcus cruris mixtum"},
    {type_name:  "Dekubitus Grad 1"},
    {type_name: "Dekubitus Grad 2"},
    {type_name:  "Dekubitus Grad 3"},
    {type_name: "Dekubitus Grad 4"},
    {type_name:  "Diabetisches Fußsyndrom"},
    {type_name: "Chronische Wunde"},
    {type_name:  "Postoperative Wundheilungsstörung"},
    {type_name:  "Mechanische Wunde"},
    {type_name:  "Pergamenthaut"},
    {type_name:  "Verbrennung"},
    {type_name: "Hautablederung/Abschürfung"},
    {type_name: "Platzwunde"},
    {type_name: "Tumorwunde"},
    {type_name:  "Anus Praeter"},
    {type_name:  "Sonde/Drainage/Katheter/Port"},
    {type_name:  "Amputation"},
    {type_name: "Kompartmentsyndrom"}
])

Factor.create([{factor_name: "Chronisch venöse Insuffizienz"},
    {factor_name: "Polyneuropathie"},
    {factor_name: "Adipositas"},
    {factor_name: "Immunsuppression"},
    {factor_name: "Kachexie"},
    {factor_name: "Diabetes Mellitus Typ 1"},
    {factor_name: "Diabetes Mellitus Typ 2"},
    {factor_name: "Diabetes Mellitus Typ 3"},
    {factor_name: "Maligner Tumor"},
    {factor_name: "Infektion systemisch/ lokal"},
    {factor_name: "Mangelernährung"},
    {factor_name: "Periphere arterielle Verschlusskrankheit (pAVK)"},
    {factor_name: "Medikamente", data_type_id: 1}
])

WoundPhase.create([{phase: "Exsudationsphase"},
    {phase: "Granulationsphase/Wundgrund"},
    {phase: "Epithelisierungsphase"},
    {phase: "Nekrotisch"}
])
WoundPhaseOption.create([{description: "Serös" , wound_phase_id: 1},
    {description: "Blutig", wound_phase_id: 1},
    {description: "Eitrig", wound_phase_id: 1},
    {description: "Fibrinös", wound_phase_id: 2},
    {description: "Biofilm", wound_phase_id: 2},
    {description: "trocken", wound_phase_id: 2},
    {description: "Blass/rosa",wound_phase_id: 2},
    {description: "feinkörnig, rot", wound_phase_id: 2},
    {description: "feste Konsistenz", wound_phase_id: 2},
    {description: "Beginnend, rosa", wound_phase_id: 3},
    {description: "Abgeschlossen", wound_phase_id: 3},
    {description: "Feucht", wound_phase_id: 4},
    {description: "Trocken", wound_phase_id: 4},
    {description: "Gangrän", wound_phase_id: 4},
    {description: "Koagulationsnekrose", wound_phase_id: 4}
])

Exudation.create([{description: "Keines"},
    {description: "Serös"},
    {description: "Serös,blutig"},
    {description: "Eitrig"},
    {description: "Grünlich"},
    {description: "Riechend"}
])

Infection.create([{description: "Mikrobiologie/Abstrich", data_type_id: 3},
    {description: "Rötung"},
    {description: "Schwellung"},
    {description: "Überwärmung"},
    {description: "Schmerz"},
    {description: "Funktionseinschränkung"}
])

WoundMargin.create([{description: "Intakt"},
    {description: "Mazeriert"},
    {description: "Gerötet"},
    {description: "Ödematos"},
    {description: "Eingerollt"},
    {description: "Unterminiert"}
])

Surrounding.create([{description: "Intakt"},
    {description: "Trocken/schuppig"},
    {description: "Rötung"},
    {description: "Ödematos"},
    {description: "Mazeriert"},
    {description: "Verhornung"},
    {description: "Pusteln"},
    {description: "Juckreiz"}
])

Debridement.create([{description: "Mechanisch"},
    {description: "Autolytisch"},
    {description: "Enzymatisch"},
    {description: "Chirurgisch"}
])

Cleansing.create([{description: "Ringerlösung"},
    {description: "Antiseptikum"}
])

SkinCondition.create([{description: "Trocken"},
    {description: "Rissig"},
    {description: "Schuppig"},
    {description: "Feucht"},
    {description: "Hautekzem"},
    {description: "Kontaktekzem"},
    {description: "Juckreiz"},
    {description: "Pusteln"}
])

Vorlage.create([{ template_name:"WFM-100-Echo LVEF", template_content: "Eine Echokardiographie der LVEF muss beim Patienten durchgeführt werden."},
{template_name:"WFM-101-Echo Klappen", template_content:"Eine Echokardiographie der Klappen muss beim Patienten durchgeführt werden."},
{template_name:"WFM-102-Echo Perikarderguss", template_content:"Kontrolle des Perikardergusses ist angefordert."},
{template_name:"WFM-110-Sono Pleura ", template_content:"Eine Sonokardiographie steht beim Patienten aus."},
{template_name:"WFM-120-Risikofaktoren", template_content:"Patient leidet an...."},
{template_name:"WFM-130-Wundversorgung", template_content:"Die Wundversorgung beim vorliegenden Patient steht noch aus."},
{template_name:"WFM-140-Laborkontrolle", template_content:"Kleines Blutbild muss gemacht werden."},
{template_name:"WFM-141-Kontrolle Nieren Diuretika", template_content:"Eine nierenkontrolle muss beim Patienten durchgeführt werden."},
{template_name:"WFM-142-Kontrolle Nieren NOKA", template_content:"Kontrolle Nieren NOKA ist geordert."},
])

SkinCare.create([{description:"Hautpflege 1"}])

Dressing.create([{description:"Wundauflage 1"}])

Cave.create([{patient_id:1, allergy: "Pollen", resistant_germs: "Staphylokokken", other: "schwerhörig" }])

Cave.create([{patient_id:2, allergy: "Tulpen", resistant_germs: "Germs", other: "taub" }])

Cave.create([{patient_id:3, allergy: "Blumen", resistant_germs: "Keime", other: "blind" }])

