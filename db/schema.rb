# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_15_082937) do

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "batch_documentations", force: :cascade do |t|
    t.string "batch_number"
    t.string "medical_product"
    t.integer "documentation_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["documentation_id"], name: "index_batch_documentations_on_documentation_id"
  end

  create_table "caves", primary_key: "patient_id", force: :cascade do |t|
    t.string "allergy"
    t.string "resistant_germs"
    t.string "other"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["patient_id"], name: "index_caves_on_patient_id"
  end

  create_table "cleansings", force: :cascade do |t|
    t.string "description"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_cleansings_on_data_type_id"
  end

  create_table "cleansings_documentations", id: false, force: :cascade do |t|
    t.integer "cleansing_id"
    t.integer "documentation_id"
    t.index ["cleansing_id"], name: "index_cleansings_documentations_on_cleansing_id"
    t.index ["documentation_id"], name: "index_cleansings_documentations_on_documentation_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "content"
    t.integer "documentation_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["documentation_id"], name: "index_comments_on_documentation_id"
  end

  create_table "data_types", force: :cascade do |t|
    t.string "data_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "debridements", force: :cascade do |t|
    t.string "description"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_debridements_on_data_type_id"
  end

  create_table "debridements_documentations", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "debridement_id"
    t.index ["debridement_id"], name: "index_debridements_documentations_on_debridement_id"
    t.index ["documentation_id"], name: "index_debridements_documentations_on_documentation_id"
  end

  create_table "documentations", force: :cascade do |t|
    t.decimal "width"
    t.decimal "depth"
    t.decimal "length"
    t.boolean "bandage_change"
    t.integer "pain"
    t.boolean "visit_doctor"
    t.boolean "permission"
    t.string "images"
    t.integer "wound_id"
    t.integer "employee_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["employee_id"], name: "index_documentations_on_employee_id"
    t.index ["wound_id"], name: "index_documentations_on_wound_id"
  end

  create_table "documentations_dressings", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "dressing_id"
    t.string "value"
    t.index ["documentation_id"], name: "index_documentations_dressings_on_documentation_id"
    t.index ["dressing_id"], name: "index_documentations_dressings_on_dressing_id"
  end

  create_table "documentations_exudations", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "exudation_id"
    t.index ["documentation_id"], name: "index_documentations_exudations_on_documentation_id"
    t.index ["exudation_id"], name: "index_documentations_exudations_on_exudation_id"
  end

  create_table "documentations_infections", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "infection_id"
    t.string "value"
    t.index ["documentation_id"], name: "index_documentations_infections_on_documentation_id"
    t.index ["infection_id"], name: "index_documentations_infections_on_infection_id"
  end

  create_table "documentations_skin_cares", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "skin_care_id"
    t.string "value"
    t.index ["documentation_id"], name: "index_documentations_skin_cares_on_documentation_id"
    t.index ["skin_care_id"], name: "index_documentations_skin_cares_on_skin_care_id"
  end

  create_table "documentations_skin_conditions", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "skin_condition_id"
    t.index ["documentation_id"], name: "index_documentations_skin_conditions_on_documentation_id"
    t.index ["skin_condition_id"], name: "index_documentations_skin_conditions_on_skin_condition_id"
  end

  create_table "documentations_surroundings", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "surrounding_id"
    t.index ["documentation_id"], name: "index_documentations_surroundings_on_documentation_id"
    t.index ["surrounding_id"], name: "index_documentations_surroundings_on_surrounding_id"
  end

  create_table "documentations_wound_margins", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "wound_margin_id"
    t.index ["documentation_id"], name: "index_documentations_wound_margins_on_documentation_id"
    t.index ["wound_margin_id"], name: "index_documentations_wound_margins_on_wound_margin_id"
  end

  create_table "dressings", force: :cascade do |t|
    t.string "description"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_dressings_on_data_type_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.boolean "doctor"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "exudations", force: :cascade do |t|
    t.string "description"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_exudations_on_data_type_id"
  end

  create_table "factors", force: :cascade do |t|
    t.string "factor_name"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_factors_on_data_type_id"
  end

  create_table "factors_wounds", id: false, force: :cascade do |t|
    t.integer "wound_id"
    t.integer "factor_id"
    t.string "value"
    t.index ["factor_id"], name: "index_factors_wounds_on_factor_id"
    t.index ["wound_id"], name: "index_factors_wounds_on_wound_id"
  end

  create_table "infections", force: :cascade do |t|
    t.string "description"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_infections_on_data_type_id"
  end

  create_table "localizations", primary_key: "wound_id", force: :cascade do |t|
    t.date "wound_since"
    t.integer "recidivism"
    t.integer "body_part"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["wound_id"], name: "index_localizations_on_wound_id"
  end

  create_table "patients", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "birth_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "phase_options", id: false, force: :cascade do |t|
    t.integer "documentation_id"
    t.integer "wound_phase_option_id"
    t.string "value"
    t.index ["documentation_id"], name: "index_phase_options_on_documentation_id"
    t.index ["wound_phase_option_id"], name: "index_phase_options_on_wound_phase_option_id"
  end

  create_table "pictures", force: :cascade do |t|
    t.string "PATH"
    t.integer "wound_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["wound_id"], name: "index_pictures_on_wound_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "author"
    t.string "content"
    t.integer "likes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reminders", force: :cascade do |t|
    t.string "text"
    t.datetime "publication_date"
    t.boolean "checked", default: false
    t.integer "patient_id"
    t.integer "documentation_id"
    t.string "justification_text"
    t.integer "counter", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["documentation_id"], name: "index_reminders_on_documentation_id"
    t.index ["patient_id"], name: "index_reminders_on_patient_id"
  end

  create_table "skin_cares", force: :cascade do |t|
    t.string "description"
    t.integer "data_types_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_types_id"], name: "index_skin_cares_on_data_types_id"
  end

  create_table "skin_conditions", force: :cascade do |t|
    t.string "description"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_skin_conditions_on_data_type_id"
  end

  create_table "surroundings", force: :cascade do |t|
    t.string "description"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_surroundings_on_data_type_id"
  end

  create_table "verifications", force: :cascade do |t|
    t.string "field"
    t.integer "employee_id"
    t.integer "documentation_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["documentation_id"], name: "index_verifications_on_documentation_id"
    t.index ["employee_id"], name: "index_verifications_on_employee_id"
  end

  create_table "vorlages", force: :cascade do |t|
    t.string "template_name"
    t.text "template_content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "wound_margins", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "wound_phase_options", force: :cascade do |t|
    t.string "description"
    t.integer "wound_phase_id"
    t.integer "data_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["data_type_id"], name: "index_wound_phase_options_on_data_type_id"
    t.index ["wound_phase_id"], name: "index_wound_phase_options_on_wound_phase_id"
  end

  create_table "wound_phases", force: :cascade do |t|
    t.string "phase"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "wound_types", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "wounddoc_pictures", force: :cascade do |t|
    t.integer "documentation_id"
    t.string "PATH"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["documentation_id"], name: "index_wounddoc_pictures_on_documentation_id"
  end

  create_table "wounds", force: :cascade do |t|
    t.boolean "permission"
    t.text "comment"
    t.string "designation"
    t.string "images"
    t.integer "patient_id"
    t.integer "wound_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["patient_id"], name: "index_wounds_on_patient_id"
    t.index ["wound_type_id"], name: "index_wounds_on_wound_type_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "batch_documentations", "documentations"
  add_foreign_key "caves", "patients"
  add_foreign_key "cleansings", "data_types"
  add_foreign_key "cleansings_documentations", "cleansings"
  add_foreign_key "cleansings_documentations", "documentations"
  add_foreign_key "comments", "documentations"
  add_foreign_key "debridements", "data_types"
  add_foreign_key "debridements_documentations", "debridements"
  add_foreign_key "debridements_documentations", "documentations"
  add_foreign_key "documentations", "employees"
  add_foreign_key "documentations", "wounds"
  add_foreign_key "documentations_dressings", "documentations"
  add_foreign_key "documentations_dressings", "dressings"
  add_foreign_key "documentations_exudations", "documentations"
  add_foreign_key "documentations_exudations", "exudations"
  add_foreign_key "documentations_infections", "documentations"
  add_foreign_key "documentations_infections", "infections"
  add_foreign_key "documentations_skin_cares", "documentations"
  add_foreign_key "documentations_skin_cares", "skin_cares"
  add_foreign_key "documentations_skin_conditions", "documentations"
  add_foreign_key "documentations_skin_conditions", "skin_conditions"
  add_foreign_key "documentations_surroundings", "documentations"
  add_foreign_key "documentations_surroundings", "surroundings"
  add_foreign_key "documentations_wound_margins", "documentations"
  add_foreign_key "documentations_wound_margins", "wound_margins"
  add_foreign_key "dressings", "data_types"
  add_foreign_key "exudations", "data_types"
  add_foreign_key "factors", "data_types"
  add_foreign_key "factors_wounds", "factors"
  add_foreign_key "factors_wounds", "wounds"
  add_foreign_key "infections", "data_types"
  add_foreign_key "localizations", "wounds"
  add_foreign_key "phase_options", "documentations"
  add_foreign_key "phase_options", "wound_phase_options"
  add_foreign_key "pictures", "wounds"
  add_foreign_key "reminders", "documentations"
  add_foreign_key "reminders", "patients"
  add_foreign_key "skin_cares", "data_types", column: "data_types_id"
  add_foreign_key "skin_conditions", "data_types"
  add_foreign_key "surroundings", "data_types"
  add_foreign_key "verifications", "documentations"
  add_foreign_key "verifications", "employees"
  add_foreign_key "wound_phase_options", "data_types"
  add_foreign_key "wound_phase_options", "wound_phases"
  add_foreign_key "wounddoc_pictures", "documentations"
  add_foreign_key "wounds", "patients"
  add_foreign_key "wounds", "wound_types"
end
