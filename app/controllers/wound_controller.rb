class WoundController < ApplicationController
    def index 
        #checks if there are ids for a patient and a employee in the url-params and saves it in variable and in a session
        if params[:Patient][:id] != "" 
            @patient = Patient.find(params[:Patient][:id])   
            session[:patient_id] = @patient.id         #session should be almost everywhere accessible 
        end 
        if params[:Employee][:id] != ""
            @employee = Employee.find(params[:Employee][:id])
            session[:employee_id] = @employee.id
        end
    end

    def new_wound
        #here we create a new woundobject
        @wound = Wound.new
    end 

    def create_wound 
        #here we use respond to to debug and see which params are passed to the controller-action
        respond_to do |f|
           f.js
           f.html{render plain: params}
        end
        #the wound object get the params from the form 
        @wound = Wound.new(wound_params)
        
        if @wound.save 
            #Manually saves the wound_id and the factor_id into the FactorsWound table by iterrating the params of the form
            #here we get the values of the params for example woundfactors =>{factors =>["",1,2], 2=> text that you have written into the input}
            params[:woundfactors][:factors][1..-1].each do |x|        
                FactorsWound.create({factor_id: x , wound_id: @wound.id,value: params[:woundfactors][x]})
            
            end



            update_cave()
            #creates a database entry in the Localization-table with the information of the params
            Localization.create({wound_id: @wound.id,body_part: params[:Localization], wound_since: params[:localization][:wound_since],recidivism: params[:localization][:recidivism]})
        end 
    end 
    def create_examination
        #this will be called if you click on "Neue Wunde anlegen because the form needs a woundobject"
        @wound = Wound.new

    end 
    def show_examination 
        #if clicked on a "Wunfaufnahme-tab" , we need the woundobject that is represented there
        @wound= Wound.find(params[:wound]) #we need the specific wound_id from the params
        respond_to do |f|
            f.html{render plain: params}
            f.js
        end
    end 
    def show_wounddoc_tab
        @wound = Wound.find(params[:wound])
    end
    def update_examination
         #here we use respond to to debug and see which params are passed to the controller-action
        respond_to do |f|
            f.js
            f.html{render plain: params}
         end
        @wound = Wound.find(params[:wound_id])  
        #that if is needed here, if you delete the last picture, after that the whole column or all pictures get deleted 
        #carrierwave has problems when you dont have pictures after deleting the last ones
        if params[:wound][:images].nil?
            @wound.remove_images!
            @wound.save!
        end 
        #normal update of a wound with new params 
        @wound.update(wound_params)

        #Manually saves the wound_id and the factor_id into the FactorsWound table by iterrating the params of the form
        #here we get the values of the params for example woundfactors =>{factors =>["",1,2], 2=> text that you have written into the input}
        @wound.factors_wounds.delete_all
        params[:woundfactors][:factors][1..-1].each do |x|          
            FactorsWound.create({factor_id: x , wound_id: @wound.id,value: params[:woundfactors][x]})
             
        end
        update_cave
        #updates the localizationentry, that is already createds
        Localization.find_by(wound_id: @wound.id).try(:update,{body_part: params[:Localization],wound_since: params[:localization][:wound_since],recidivism: params[:localization][:recidivism]})
    end 
    
    #is called if you clicked on "Neues Datum"
    def show_create_documentation
        respond_to do |f|
            f.js
            f.html{render plain: params}
         end 
         #new documentationobject created
        @documentation = Documentation.new
        #finds the wound that will get the newly created documentation
        @wound = Wound.find(params[:wound])

        #this line searches the last documentation from the wound.This is needed for a prefilled documentation
        @doc_last = @wound&.documentations.try(:last)
       
    end
    
    def batchdoc(boolean)
        #in update documentation the old batchdocs must be deleted , so there wont be duplicates
        if boolean
            BatchDocumentation.where(documentation_id: @documentation.id).delete_all
        end 
        # checks if the param batchdoc even exists
        if  params[:batchdoc] != nil
            @medproducts = params[:batchdoc][:medproducts] 
            @batchnrs = params[:batchdoc][:batchnrs]
            # zip combines two arrays so we have two string arrays ,one for medproducts and one for batchnrs example [["Gel","56165"],["Tuch","2121651"]]
            @medproducts.zip(@batchnrs).each do |medproduct,batchnr|
                #empty batchdocs wont be saved
                if medproduct != "" || batchnr != ""
                    BatchDocumentation.create(medical_product: medproduct , batch_number: batchnr , documentation_id: @documentation.id)
                end 
            end
        end
    end
    def create_documentation
        #here we need to save the documentation first , so that we have an id for the verification
        @documentation = Documentation.new(wound_id: params[:documentation][:wound_id])
        #we save the documentation here immediately because its important for the verifications
        @documentation.save
        #@documentation = Documentation.new(documentation_params) 
        @wound = Wound.find(params[:documentation][:wound_id]) 
        verification(true)
        #woundphases are saved in a extra-table between documentation and woundphase_Options, so every checkbox needs to be created manually
        params[:woundphases][:wound_phase_options].reject{|x| x==""}.each do |x|       
            PhaseOption.create({wound_phase_option_id: x , documentation_id: @documentation.id})
        end
        #the extra table between infections and documentation is something special. It has its own model because it has an extra column for values, it can save the disabled input fields form the checkboxes
        params[:infections][:infections_ids].reject{|x| x==""}.each do |x|       
            DocumentationsInfection.create({infection_id: x , documentation_id: @documentation.id, value: params[:infections][x]})
        end
        #same as woundphases
        params[:documentation][:skin_care_ids].reject{|x| x==""}.each do |x|       
             DocumentationsSkinCare.create({skin_care_id: x , documentation_id: @documentation.id})
        end

        batchdoc(false)
        Localization.find_by(wound_id: @wound.id).try(:update,{body_part: params[:Localization]})
        #the documentation must be updated after the creation if verifications, because the verification methdd compares old values with new values
        @documentation.update(documentation_params)
        #the comments are saved manually and the ActionTextRichText-Model is created by us, so we can save comments on our own
        params[:comments]&.each do |c| 
            ActionTextRichText.create(name: "bemerkung", body: c , record_type: "Documentation", record_id: @documentation.id)
        end
        #params[:reminders] contains an array that is built like this [text,time], so we need to iterrate every two items of the array 
        params[:reminders]&.each_slice(2) do |r,z|  
            Reminder.create(text: r, publication_date: z , documentation_id: @documentation.id , patient_id: session[:patient_id])
        end


        


    end
    def show_documentation
        @documentation = Documentation.find(params[:documentation])
    end 
    def update_documentation
        #get all params if the show_view_form
        @documentation = Documentation.find(params[:documentation][:id])
        @wound = @documentation.wound_id
        verification(false)
        #same principle as in the wound
        if params[:documentation][:images].nil?
            @documentation.remove_images!
            @documentation.save!
        end 
        #the documentation must be updated after the verification_method, so the verification method still has access to the old values of te documentation
        @documentation.update(documentation_params)
        batchdoc(true)
        #instead of searching each id and updating each record , we drop all checkboxes of the documentation and create each one again
        @documentation.documentations_infections.delete_all
        params[:infections][:infections_ids].reject{|x| x==""}.each do |x|       
            DocumentationsInfection.create({infection_id: x , documentation_id: @documentation.id, value: params[:infections][x]})
        end
         #instead of searching each id and updating each record , we drop all checkboxes of the documentation and create each one again
        @documentation.documentations_skin_cares.delete_all
        params[:documentation][:skin_care_ids].reject{|x| x==""}.each do |x|       
            DocumentationsSkinCare.create({skin_care_id: x , documentation_id: @documentation.id})
        end
         #instead of searching each id and updating each record , we drop all checkboxes of the documentation and create each one again
        @documentation.phase_options.delete_all
        params[:woundphases][:wound_phase_options].reject{|x| x==""}.each do |x|       
            PhaseOption.create({wound_phase_option_id: x , documentation_id: @documentation.id})
        end
        Localization.find_by(wound_id: @wound).try(:update,{body_part: params[:Localization]})
        #same as in the create_documentation
        params[:comments]&.each do |c| 
            ActionTextRichText.create(name: "bemerkung", body: c , record_type: "Documentation", record_id: @documentation.id)
        end
        #same as in the create_documentation
        params[:reminders]&.each_slice(2) do |r,z|  
            Reminder.create(text: r, publication_date: z , documentation_id: @documentation.id ,patient_id: session[:patient_id])
        end
    end
   
    private 
        def wound_params
            #permits all params that are needed for a wound_object
            params.require(:wound).permit(:wound_type_id,:patient_id,:designation,:comment,{images:[]}, :permission)
        end
        def documentation_params
             #permits all params that are needed for a documentation_object
            params.require(:documentation).permit(:id,:wound_id, :pain, :width,:depth,:length,:bandage_change,:visit_doctor,:permission,:employee_id,{images:[]},exudation_ids:[], infection_ids:[], surrounding_ids:[], wound_margin_ids:[], debridement_ids:[], skin_condition_ids:[],skin_care_ids:[], cleansing_ids:[], dressing_ids:[])
        end

        def update_cave
            #here we search with patient_id the cave_record that belongs to the patient and create or update it 
            if Cave.find_by(patient_id: params[:wound][:patient_id])==nil
                Cave.create(allergy: params[:cave][:allergy], resistant_germs: params[:cave][:resistant_germs], other: params[:cave][:other], patient_id: params[:wound][:patient_id])
            else
                Cave.find(params[:wound][:patient_id]).update(allergy: params[:cave][:allergy], resistant_germs: params[:cave][:resistant_germs], other: params[:cave][:other])

            end
        end

        def cave_params
             ret = params.require(:cave).permit(:allergy, :resistant_germs, :other)
            return ret
        end

        def comment_params
            params.require(:comment).permit(:title, :content)
        end

        def reminder_params
            params.require(:reminder).permit(:text, :publication_date)
        end
    public 
        def verification(boolean)
            #if booelan is true, then the function will get called in the create_documentation
            #the idea here is to differentiate bewteen the verification in the create_documentation and the update/show_documentation
            #in the if statement we create every verification that is needed without comparing to the old record because they arent old records available 
            #here we get all information from the params because in the params are the newest data
            if boolean
                Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id, field: "Dokumentation erstellt")
                Array({"Breite"=>if params[:documentation][:width] =="" then  "leer" else params[:documentation][:width] end ,"Tiefe"=>if params[:documentation][:depth] =="" then  "leer" else params[:documentation][:depth] end ,"Länge"=>if params[:documentation][:length] =="" then  "leer" else params[:documentation][:length] end }).each do |ws|
                    if (ws[1]!="leer")
                        Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id, field: "Wundgröße:#{ws[0]}->#{ws[1]}")
                    end
                end
                Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id,field: "Wundschmerzskala -> #{params[:documentation][:pain]}")
                if params[:documentation][:permission] == "true"
                    Verification.create(employee_id: session[:employee_id],documentation_id: @documentation.id, field: "Einverständnis für die Fotoaufnahme -> ausgewählt")
                else
                    Verification.create(employee_id: session[:employee_id],documentation_id: @documentation.id, field: "Einverständnis für die Fotoaufnahme -> nicht ausgewählt") 
                end 
                params[:infections][:infections_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Wundinfektion ->#{Infection.find(x).description}")
                end 
                params[:documentation][:wound_margin_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Wundrand->#{WoundMargin.find(x).description}")
                end
                if @documentation.bandage_change != params[:documentation].try(:[],:bandage_change)
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{if params[:documentation][:bandage_change] == "true" then "Wundschmerz-> Immer" else "Wundschmerz->Nur bei Verbandswechsel" end }")
                end
                if Localization.find_by(wound_id: @wound).body_part.to_s != params[:Localization].to_s
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field:"Körperteil -> #{params[:Localization].to_s}" )
                end
                params[:woundphases][:wound_phase_options].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{WoundPhase.find(WoundPhaseOption.find_by(id:x).wound_phase_id).phase}->#{WoundPhaseOption.find_by(id:x).description}")
                end
                params[:documentation][:exudation_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Wundexsudation->#{Exudation.find(x).description}")
                end
                params[:documentation][:skin_condition_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Hautzustand->#{SkinCondition.find(x).description}")
                end
                params[:documentation][:debridement_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Debridement->#{Debridement.find(x).description}")
                end 
                params[:documentation][:surrounding_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Wundumgebung->#{Surrounding.find(x).description}")
                end
                params[:documentation][:skin_care_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Hautpflege->#{SkinCare.find(x).description}")
                end
                params[:documentation][:cleansing_ids].reject{|x| x==""}.each do |x|
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "Wundspülung/-reinigung->#{Cleansing.find(x).description}")
                end    
                if params[:batchdoc] != nil
                    params[:batchdoc][:medproducts].zip(params[:batchdoc][:batchnrs]).each do |medproduct,batchnr|
                        if medproduct != "" || batchnr != ""
                            Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id, field: "Chargendokument -> #{medproduct}-#{batchnr}")
                        end 
                    end
                end 
            # this else will be called if the boolean is false. Thats the case in the update_documentation
            else
            #the idea here is to get the old data of a documentation from the database and compare it to the new data in the params 
            # if the params_data is not equal to the database_data, then we create new verifications
                if params[:documentation][:pain] != @documentation.pain.to_s 
                    Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id,field: "Wundschmerzskala -> #{params[:documentation][:pain]}")
                end 
                if @documentation.permission.to_s != params[:documentation][:permission]
                    if params[:documentation][:permission] == "true"
                        Verification.create(employee_id: session[:employee_id],documentation_id: @documentation.id, field: "Einverständnis für die Fotoaufnahme -> ausgewählt")
                    else
                        Verification.create(employee_id: session[:employee_id],documentation_id: @documentation.id, field: "Einverständnis für die Fotoaufnahme -> nicht ausgewählt") 
                    end 
                   
                end
                Array({"Breite"=>if params[:documentation][:width] =="" then  "leer" else params[:documentation][:width] end ,"Tiefe"=>if params[:documentation][:depth] =="" then  "leer" else params[:documentation][:depth] end ,"Länge"=>if params[:documentation][:length] =="" then  "leer" else params[:documentation][:length] end }).zip([if @documentation.width ==nil then "leer" else  @documentation.width end,if @documentation.depth == nil then "leer" else   @documentation.depth end,if @documentation.length ==nil then "leer" else  @documentation.length end]).each do |ws|
                    if ws[0][1] != ws[1].to_s
                        Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id, field: "Wundgröße:#{ws[0][0]}->#{ws[0][1]}")
                    end 
                end
                if (if @documentation.bandage_change==nil then "nil" else @documentation.bandage_change ? "true" : "false" end)!= (params[:documentation].try(:[],:bandage_change) ? params[:documentation].try(:[],:bandage_change) : "nil")
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{if params[:documentation][:bandage_change] == "true" then "Wundschmerz-> Immer" else "Wundschmerz->Nur bei Verbandswechsel" end }")
                end
                if Localization.find_by(wound_id: @wound).body_part.to_s != params[:Localization].to_s
                    Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field:"Körperteil -> #{params[:Localization].to_s}" )
                end
                #Phases are special checkboxes in comparison to others 
                #principle is explained in the comparison method
                @old_phase_boxes = @documentation.phase_options.pluck(:wound_phase_option_id)
                @new_phase_boxes = params[:woundphases][:wound_phase_options].reject{|x| x==""}
                if @old_phase_boxes.empty? && @new_phase_boxes.empty?
                elsif @old_phase_boxes.empty? && !@new_phase_boxes.empty?
                    @new_phase_boxes.each do |npb|
                        Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{WoundPhase.find(WoundPhaseOption.find_by(id:npb).wound_phase_id).phase}->#{WoundPhaseOption.find_by(id:npb).description}")
                    end 
                elsif !@old_phase_boxes.empty? && @new_phase_boxes.empty?
                    @old_phase_boxes.each do |opb|
                        Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{WoundPhase.find(WoundPhaseOption.find_by(id:opb).wound_phase_id).phase}->#{WoundPhaseOption.find_by(id:opb).description} |entfernt|")
                    end 
                elsif @old_phase_boxes.length  > @new_phase_boxes.length
                    @hash1 = @new_phase_boxes.map{|x| [x,true]}.to_h
                    @old_phase_boxes.each do |ne|
                        if @hash1[ne.to_s].nil?
                            Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{WoundPhase.find(WoundPhaseOption.find_by(id:ne).wound_phase_id).phase}->#{WoundPhaseOption.find_by(id:ne).description} |entfernt|")
                        else    
                            @hash1.delete(ne.to_s)
                            
                        end 
                    end 
                    if !@hash1.empty?
                        @hash1.each do |key|
                            Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{WoundPhase.find(WoundPhaseOption.find_by(id:key[0]).wound_phase_id).phase}->#{WoundPhaseOption.find_by(id:key[0]).description}")
                        end
                    end
                else
                    @hash2 = @old_phase_boxes.map{|x| [x,true]}.to_h
                    @new_phase_boxes.each do |old|
                        if @hash2[old.to_i].nil?
                            Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id,field: "#{WoundPhase.find(WoundPhaseOption.find_by(id: old).wound_phase_id).phase}->#{WoundPhaseOption.find_by(id:old).description}")
                        else 
                            @hash2.delete(old.to_i)
                        end
                    end 
                    if !@hash2.empty?
                        @hash2.each do |key|
                            Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{WoundPhase.find(WoundPhaseOption.find_by(id:key[0]).wound_phase_id).phase}->#{WoundPhaseOption.find_by(id:key[0]).description} |entfernt|")
                        end
                    end
    
                end  
                #batchdoc is special as well , so you cant use the comparison method
                #principle is explained in the comparison method
                @old_batch = @documentation.batch_documentations.pluck(:medical_product,:batch_number) 
                @new_batch = params[:batchdoc].try(:[],:medproducts)&.zip(params[:batchdoc][:batchnrs])
                if @old_batch.empty? && @new_batch.nil?
                elsif @old_batch.empty? && !@new_batch.nil?
                    @new_batch&.each do |nb|
                        Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id, field: "Chargendokument -> #{nb[0]}-#{nb[1]}")
                    end
                elsif !@old_batch.empty? && @new_batch.nil? 
                    @old_batch.each do |ob|
                        Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id, field: "Chargendokument -> #{ob[0]}-#{ob[1]} |entfernt|")
                    end
                elsif @old_batch.length > (@new_batch.nil? ? 0 : @new_batch.length) 
                    @batchhash1 = @new_batch.map{|x| [x,true]}.to_h
                    @old_batch.each do |ob|
                        if @batchhash1[ob].nil?
                            Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id, field: "Chargendokument -> #{ob[0]}-#{ob[1]} |entfernt|")
                        else 
                            @batchhash1.delete(ob)
                        end
                    end
                    if !@batchhash1.empty?
                        @batchhash1.each do |key|
                            Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id, field: "Chargendokument -> #{key[0][0]}-#{key[0][1]}")
                        end
                    end
                else 
                    @batchhash2 = @old_batch.map{|x| [x,true]}.to_h
                    @new_batch.each do |nb|
                        if @batchhash2[nb].nil?
                            Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id, field: "Chargendokument -> #{nb[0]}-#{nb[1]}")
                        else 
                            @batchhash2.delete(nb)
                        end
                    end 
                    if !@batchhash2.empty?
                        @batchhash2.each do |key|
                            Verification.create(employee_id: session[:employee_id], documentation_id: @documentation.id, field: "Chargendokument -> #{key[0][0]}-#{key[0][1]} |entfernt|")
                        end
                    end 
                end
                    comparison(@documentation.exudations.pluck(:exudation_id),params[:documentation][:exudation_ids].reject{|x| x==""},"Wundexsudation",Exudation)
                    comparison(@documentation.skin_conditions.pluck(:skin_condition_id),params[:documentation][:skin_condition_ids].reject{|x| x==""},"Hautzustand",SkinCondition)
                    comparison(@documentation.infections.pluck(:infection_id),params[:infections][:infections_ids].reject{|x| x==""},"Wundinfektion",Infection)
                    comparison(@documentation.wound_margins.pluck(:wound_margin_id),params[:documentation][:wound_margin_ids].reject{|x| x==""},"Wundrand",WoundMargin)
                    comparison(@documentation.debridements.pluck(:debridement_id),params[:documentation][:debridement_ids].reject{|x| x==""},"Debridement",Debridement)
                    comparison(@documentation.surroundings.pluck(:surrounding_id),params[:documentation][:surrounding_ids].reject{|x| x==""},"Wundumgebung",Surrounding)
                    comparison(@documentation.skin_cares.pluck(:skin_care_id),params[:documentation][:skin_care_ids].reject{|x| x==""},"Hautpflege",SkinCare)
                    comparison(@documentation.cleansings.pluck(:cleansing_id),params[:documentation][:cleansing_ids].reject{|x| x==""},"Wundspülung/-reinigung",Cleansing)
                    comparison(@documentation.dressings.pluck(:dressing_id),params[:documentation][:dressing_ids].reject{|x| x==""}, "Wundauflage",Dressing)
            end 
        #ends method verification(boolean)    
        end
        
        #for this method, it is important that the model has  a column named "desctiption", else the method doesnt work or you need to add param that displays the column name
        def comparison(old_array,new_array,string,model) 
            #here we need to check all cases empty|empty , empty|not empty| , more checkboxes than before , its important for the logic
                    if old_array.empty? && new_array.empty?
                    elsif old_array.empty? && !new_array.empty?
                        new_array.each do |npb|
                            Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{string}->#{model.find(npb).description}")
                        end 
                    elsif !old_array.empty? && new_array.empty?
                        old_array.each do |opb|
                            Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{string}->#{model.find(opb).description} |entfernt|")
                        end 
                    elsif old_array.length  > new_array.length
                        #here we create a hash with the smaller array, so we can check every checkbox with every other checkbox
                        @hash1 = new_array.map{|x| [x,true]}.to_h
                        #if the checkbox doesnt exist in the hash it msut be deleted or newly created
                        #if the checkbox from the bigger array exists in the hash, the checkbox gets deleted from the hash, so the rest of the checkboxes in the hash will be represeted into new verifications
                        old_array.each do |ne|
                            if @hash1[ne.to_s].nil?
                                Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{string}->#{model.find(ne).description} |entfernt|")
                            else    
                                @hash1.delete(ne.to_s) 
                            end 
                        end 
                        #here we read the rest of the checkboxes that are newly checked or deleted, the hash contains these
                        if !@hash1.empty?
                            @hash1.each do |key|
                                Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{string}->#{model.find(key[0]).description}")
                            end
                        end
                    else
                        #same principle only the other way around
                        @hash2 = old_array.map{|x| [x,true]}.to_h
                        new_array.each do |old|
                            if @hash2[old.to_i].nil?
                                Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id,field: "#{string}->#{model.find(old).description}")
                            else 
                                @hash2.delete(old.to_i)
                            end
                        end 
                        if !@hash2.empty?
                            @hash2.each do |key|
                                Verification.create(employee_id: session[:employee_id] , documentation_id: @documentation.id , field: "#{string}->#{model.find(key[0]).description} |entfernt|")
                            end
                        end
                    end 
        #here ends the method comparison                    
        end 
end
