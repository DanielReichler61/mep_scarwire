class RemindersController < ApplicationController

    def get_reminders
        #an ajax-request calls this action every 15seconds so it can get the newly remidner that are ToDo
        #here we get all reminders of the specific patient and its publication_date is smaller than the current time now
        @reminders_to_do = Reminder.where("patient_id=? AND checked = ? AND publication_date <= ? ",session[:patient_id],false,DateTime.now).order("publication_date DESC")
    end
    def get_all_reminders_from_patient
        #herer we get all reminders of one patient and sort them after time
        @patient_reminders =  Reminder.where("patient_id=?",session[:patient_id]).order("publication_date ASC")
    end 
    def get_all_reminders
        # here we get the whole reminder_table and sort them afte checked and time
        @reminders_from_all_patients = Reminder.all.order("checked ASC").order("publication_date ASC")
    end
    def update_reminder 
        @reminder = Reminder.find(params[:reminder][:id])
        # check if the reminder is checked or unchecked
        if params[:reminder][:checked] == "true" || params[:reminder][:justification_text] ==nil
            @reminder.update(checked: params[:reminder][:checked])
        else
            #here the reminder gets postponed
            if params[:reminder][:publication_date] == @reminder.publication_date ||  params[:reminder][:publication_date] == ""
                @reminder.update(publication_date: @reminder.publication_date + 1.days,justification_text: params[:reminder][:justification_text],counter: @reminder.counter+1)
            else 
                @reminder.update(publication_date: params[:reminder][:publication_date],justification_text: params[:reminder][:justification_text],counter: @reminder.counter+1)
            end
        end 
        @reminders_to_do = Reminder.where("patient_id=? AND checked = ? AND publication_date <= ? ",session[:patient_id],false,DateTime.now).order("publication_date DESC")
        @reminders_from_all_patients = Reminder.all.order("checked ASC").order("publication_date ASC")
        @patient_reminders =  Reminder.where("patient_id=?",session[:patient_id]).order("publication_date ASC")
    end 

    private 
    def reminder_params
        params.require(:reminder).permit(:checked,:publication_date,:justification_text)
    end
end
