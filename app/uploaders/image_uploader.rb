class ImageUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    if model.is_a?(Wound)
      "uploads/Patient_#{model.patient_id}/Wunde_#{model.id}/Wundaufnahme"
      #uploads/patient_1/wound/1/wundaufnahme/something.png 
    elsif model.is_a?(Documentation) 
      "uploads/Patient_#{Wound.find(model.wound_id).patient_id}/Wunde_#{model.wound_id}/Dokumentation_#{model.id}"
      #uploads/patient_1/wound/1/documentation/1/something.png
    end     
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url(*args)
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process resize_to_fit: [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_whitelist
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    if @original_filename.include? "image"
      @original_filename = SecureRandom.hex(8)
    end 
    "#{Date.today.strftime("%d.%m.%Y")}-#{secure_token}.#{file.extension}" if @original_filename.present?
   
  end
  protected 
  #this method is for giving the filenames random names and also looking that the same file doesnt get 2 different names
  def secure_token
    media_original_filenames_var = :"@#{mounted_as}_original_filenames"
  
    unless model.instance_variable_get(media_original_filenames_var)
      model.instance_variable_set(media_original_filenames_var, {})
    end
  
    unless model.instance_variable_get(media_original_filenames_var).map{|k,v| k }.include? original_filename.to_sym
      new_value = model.instance_variable_get(media_original_filenames_var).merge({"#{original_filename}": SecureRandom.hex(8)})
      model.instance_variable_set(media_original_filenames_var, new_value)
    end
  
    model.instance_variable_get(media_original_filenames_var)[original_filename.to_sym]
  end 
end
