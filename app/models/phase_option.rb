class PhaseOption < ApplicationRecord
    belongs_to :documentation
    belongs_to :wound_phase_option
end
