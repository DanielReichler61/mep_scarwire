class WoundPhaseOption < ApplicationRecord
    has_many :phase_options
    has_many :documentations, through: :phase_options
    belongs_to :wound_phase
    belongs_to :data_type , optional: true
end
