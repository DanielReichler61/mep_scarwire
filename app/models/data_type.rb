class DataType < ApplicationRecord
    has_many :skin_cares
    has_many :exudations
    has_many :infections 
    has_many :dressings
    has_many :wound_phase_options
    has_many :skin_conditions
    has_many :debridements
    has_many :cleansings
    has_many :surroundings 
    has_many :factors 
end
