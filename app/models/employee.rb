class Employee < ApplicationRecord
    has_many :verifications
    has_many :documentations

    def info
        "#{first_name} #{last_name}" 
    end 
end
