class Factor < ApplicationRecord
    #has_and_belongs_to_many :wounds
    has_many :factors_wounds
    has_many :wounds, through: :factors_wounds
    
    belongs_to :data_type , optional: true
end
