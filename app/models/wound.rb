class Wound < ApplicationRecord
    belongs_to :patient
    has_many :documentations
    has_one :localization
    has_many :pictures 
    has_many :factors_wounds
    has_many :factors, through: :factors_wounds 
    mount_uploaders :images, ImageUploader
    serialize :images , JSON
    has_rich_text :comment
    belongs_to :wound_type
    accepts_nested_attributes_for :factors_wounds
end
