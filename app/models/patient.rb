class Patient < ApplicationRecord
    has_many :wounds 
    has_many :reminders
    has_one :cave

    def info
        "#{first_name} #{last_name} #{birth_date.strftime("%d.%m.%Y")}"  #strftime changes the date format
    end 
end
