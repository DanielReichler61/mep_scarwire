class Infection < ApplicationRecord
    has_many :documentations_infections
    has_many :documentations, through: :documentations_infections
    belongs_to :data_type , optional: true
end
