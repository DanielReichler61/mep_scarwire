class Documentation < ApplicationRecord
    belongs_to :wound
    has_and_belongs_to_many :cleansings
    has_and_belongs_to_many :debridements
    has_and_belongs_to_many :skin_conditions
    has_many :phase_options
    has_many :wound_phase_options, through: :phase_options
    has_and_belongs_to_many :surroundings
    has_and_belongs_to_many :dressings
    has_many :documentations_infections
    has_many :infections, through: :documentations_infections
    has_many :documentations_skin_cares
    has_many :skin_cares, through: :documentations_skin_cares
    has_and_belongs_to_many :exudations
    has_many :batch_documentations
    has_rich_text :comment_doc
    has_many :verifications
    mount_uploaders :images, ImageUploader
    serialize :images , JSON
    has_many :reminders
    belongs_to :employee ,optional: true
    has_and_belongs_to_many :wound_margins
end

