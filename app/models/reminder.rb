class Reminder < ApplicationRecord
    belongs_to :patient, optional: true 
    belongs_to :documentation , optional: true 
end
