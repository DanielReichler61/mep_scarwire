class Comment < ApplicationRecord
    belongs_to :documentation
    has_rich_text :content
end
