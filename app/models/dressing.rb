class Dressing < ApplicationRecord
    has_and_belongs_to_many :documentations
    belongs_to :data_type , optional: true
end
