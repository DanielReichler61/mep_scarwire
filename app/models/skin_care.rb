class SkinCare < ApplicationRecord
    has_many :documentations_skin_care
    has_many :documentations, through: :documentations_skin_cares
    belongs_to :data_types , optional: true
end
