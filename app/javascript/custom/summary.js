console.log("Zusammenfassung")
$(document).on("change","#Wundexsudation,#Wundinfektion,#Wundränder,#Wundumgebung,#Debridement,#Wundspülung,#Hautzustand,#Hautpflege,#Wundauflage,#EinverständnisvomPatient,#WundvisitemitArzt",function(){
    let id = event.target.id
    let identifier = id.slice(0,-2)
    let td_id = $(event.target).parent().parent().attr("id")
    let checked = $("input[id*='"+identifier+"']:checked")
    let stringcontent ="";
    $("td:contains("+td_id+")").parent().prop("hidden",false)
    for(let i = 0;i<checked.length;i++){
        if (($("label[for='"+ checked[i].id + "']").html()=="Mikrobiologie/Abstrich") && ($("#infections_1").val()!="")){
            let split = $("#infections_1").val().split("-")
            stringcontent+=$("label[for='"+ checked[i].id + "']").html() + ":"+split[2]+"."+split[1]+"."+split[0] + "|"
        }else{
             stringcontent+=$("label[for='"+ checked[i].id + "']").html() + "|"
        }
    }
    $("td:contains("+td_id+") + td").html(stringcontent)
})
$(document).on("change","#Exsudationsphase,#Granulationsphase,#Epithelisierungsphase,#Nekrotisch",function(){
    let  id = event.target.id
    let identifier = id.slice(0,-2)
    let td_id = $(event.target).parent().parent().attr("id")
    let checked = $("#"+td_id+ " input[type='checkbox']:checked")
    $("td:contains("+td_id+")").parent().prop("hidden",false)
    let stringcontent = "";
    for(let i = 0; i<checked.length;i++){
        stringcontent+=$("label[for='"+ checked[i].id + "']").html() + "|"  
    }
    $("td:contains("+td_id+") + td").html(stringcontent)

})
$(document).on("change","#batchlist",function(){
    let td_id = "Chargendokumentation"
    let products = $("#batchlist tbody tr")
    $("td:contains("+td_id+")").parent().prop("hidden",false)
    let stringcontent = ""
    for(let i = 0; i<products.length;i++){
        stringcontent+= products[i].getElementsByTagName("input")[0].value + "-"+products[i].getElementsByTagName("input")[1].value + "|"
    }
    $("td:contains("+td_id+") + td").html(stringcontent)
})
$(document).on("change","#width,#depth,#length",function(){
    let td_id   = "Wundgröße"
    $("td:contains("+td_id+")").parent().prop("hidden",false)
    $("span[id='tdbreite']").each(function(){
        $(this).html($("#width").val())
    })
    $("span[id='tdtiefe']").each(function(){
        $(this).html($("#depth").val())
    })
    $("span[id='tdlänge']").each(function(){
        $(this).html($("#length").val())
    })
}
)
$(document).on("change","#myRange,#documentation_bandage_change_true,#documentation_bandage_change_false",function(){
    let td_id = "Wundschmerz"
    $("td:contains("+td_id+")").parent().prop("hidden",false)
    let identifier=$("input[id*='documentation_bandage_change']:checked").attr("id") 
    $("span[id='radiopain']").each(function(){
        $(this).html( $("label[for='"+ identifier + "']").html())
    })
    let scalevalue = document.getElementById("myRange").value
    $("span[id='skala']").each(function(){
        $(this).html(scalevalue)
    })
})
