let formnumber = 0
$(document).on("click","#close_remdinder_popup",function(){
    $("#reminder_modal_popup").hide()
    formnumber = $("#pills-home #reminder_body tr").length
})
//after loading the page an ajax call will be made it calls the action in the reminders controller
$(document).ready(function(){
  $.ajax({
    url: "/reminders/get_reminders",
    type: "get",
  })

})

function doAJAX(){
  //stops ajax calls when the modal is open
        if( $('#reminder_modal_popup').css('display') == 'none' ) {
            $.ajax({
            url: "/reminders/get_reminders",
            type: "get"
          })
          //if there arent reminders the little red notification bell will be hidden
          if($("#pills-home #reminder_body tr").length ==0) {
            $("#reminder_notification_number").prop("hidden",true)
          }else{
            $("#reminder_notification_number").prop("hidden",false)
            $("#reminder_notification_number").html($("#pills-home #reminder_body tr").length)
          }
          //if there are more reminders than before then let the modal appear
            if (formnumber < $("#pills-home #reminder_body tr").length){
              $("#open_reminder_modal").click()
              $("#popuped_reminders").click()
             
       }
         }
       
}
//every 15 seconds the remidners get checked
setInterval(doAJAX,7500)
// if a reminders checkbox is checked then an ajax call will be made
$(document).on("click","#pills-tabContent #reminder_checked",function(){
  if (event.target.checked){
    $.ajax({
      url: "/reminders/update_reminder",
      type: "post",
      data: {reminder: {id: $(event.target).parent().parent().find("input[type='hidden']").val(),checked: "true"}}
    })
  }
  else{
    $.ajax({
      url: "/reminders/update_reminder",
      type: "post",
      data: {reminder: {id: $(event.target).parent().parent().find("input[type='hidden']").val(),checked: "false"}}
    })
  }
})

//by clicking on the postpone button the content will be show or not 
$(document).on("click","#post_pone_button",function(){
  if($(event.target).parent().prev().find("input").is(":checked")){
    let span = $(document.createElement("span"))
    $(span).attr("id","postpone_fail")
    $(span).css("color","red")
    $(span).html("Kein Verschieben möglich. Erinnerung ist erledigt")
    $(event.target).after(span)
    $(document).one("click",document,function(){
      $("#postpone_fail").remove()
    })
  }else{
    $(event.target).next().prop("hidden",!$(event.target).next().prop("hidden"))
  }
  
})

$(document).on("click","#postpone_reminder_save",function(){
  console.log($("#justification_text").val()=="")
  console.log($(event.target).prev().prev().val())
  if($(event.target).prev().val() == ""){
    let span = $(document.createElement("span"))
    $(span).attr("id","postpone_fail")
    $(span).css("color","red")
    $(span).html("Verschieben benötigt eine Begründung")
    $(event.target).after(span)
    $(document).one("click",document,function(){
      $("#postpone_fail").remove()
    })
  }
    else{
       $.ajax({
    url: "/reminders/update_reminder",
    type: "post",
    data: {reminder: {publication_date:$(event.target).prev().prev().val(),justification_text:$(event.target).prev("textarea").val() ,id: $(event.target).parent().parent().parent().find("input[type='hidden']").val()}}
  })
    }
})
// loads information by clicking on the bell button
$(document).on("click","#open_reminder_modal",function(){
    $.ajax({
      url: "/reminders/get_all_reminders_from_patient",
      type: "get",
    })
    $.ajax({
      url: "/reminders/get_all_reminders",
      type: "get"
        })
  })
  // filter for the last tab in the remindermodal for searching for a specfic patient
  $(document).on("keyup","#search_filter_table",function(){
    let input = document.getElementById("search_filter_table")
    let filter = input.value.toUpperCase()
    let tbody  = document.getElementById("reminders_from_all_patients")
    let trs = tbody.getElementsByTagName("tr")
    let txtValue;
    for (i=0 ; i<trs.length;i++){
      td = trs[i].getElementsByTagName("td")[0];
      if(td){
        txtValue = td.textContent || td.innerText; 
        if(txtValue.toUpperCase().indexOf(filter)> -1){
          trs[i].style.display ="";
        }else{
          trs[i].style.display ="none"
        }
      }
    }

  })