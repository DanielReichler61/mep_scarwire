function openDialog(dialog2){
	if (dialog2 == null) return 
    dialog2.classList.add('active')
    console.log(document.getElementById("overlay"))
	document.getElementById("overlay").classList.add('active')
}

function closeDialog(dialog2){
    if (dialog2 == null) return 
    console.log(dialog2)
    dialog2.classList.remove('active')
    console.log(document.getElementById("overlay"))
    document.getElementById("overlay").classList.remove('active')
}
$(document).on("click", "#new-reminder" , function(){ 
    console.log(document.getElementById("dialog2"))
	let dialog2 = document.getElementById("dialog2")
	openDialog(dialog2)
})

$(document).on("click", "#overlay" , function(){ 
	const dialogs=document.querySelectorAll('.dialog2 active')
	dialogs.forEach(dialog2 => {
		closeDialog(dialog2)
	})
})


$(document).on("click", "#reminder-close-button" , function(){ 
    let dialog2 = document.getElementById("dialog2")
		closeDialog(dialog2)
})
//same principle as in the comment js 
$(document).on("click", "#save-reminder" , function(){ 
    console.log( $("#wound_reminder")) 
    let text =  $("#reminder_text_field").val()
    let publication_date =  $("#reminder_publication_date").val()
    console.log(publication_date)
    if (publication_date.length ==0){
        let span = $(document.createElement("span"))
        console.log("publication_date")
        $(span).attr("id", "date_reminder_fail");
        $(span).html("Datum wurde nicht ausgewählt.");
        $(span).css("color","red");
        $(event.target).after(span);
        $(document).one("click",document, function(){
            $(span).remove();
        })
        return;
    }

    console.log(text)
    if (text.length ==0){
        let span2 = $(document.createElement("span"))
        console.log("text")
        $(span2).attr("id", "text_reminder_fail");
        $(span2).html("Bitte Erinnerungstext eingeben!");
        $(span2).css("color","red");
        $(event.target).after(span2);
        $(document).one("click",document, function(){
            $(span2).remove();
        })
        return;
    }
    
    console.log(text)
    console.log(publication_date)
    
    let input = $(document.createElement("input"))
    input.attr("value", text);
    input.attr("type", "text");
    input.attr("name", "reminders[]");
    input.attr("hidden", "hidden")

    let input2 = $(document.createElement("input"))
    input2.attr("value", publication_date);
    input2.attr("type", "datetime-local");
    input2.attr("name", "reminders[]");
    input2.attr("hidden", "hidden")

    let td1 = $(document.createElement("td"))
    let td2 = $(document.createElement("td"))
    let tr1 = $(document.createElement("tr"))


    $("#reminderlist tr").first().after(tr1)
    $(td1).html(text)
    $(tr1).append(td1)

    $(td2).html(publication_date)
    $(td2).append(input2)

    $(td1).after(td2)
    $(td1).append(input)

    $("#reminder_text_field").val(" ")
    $("#reminder_publication_date").val(" ")
    let dialog2 = document.getElementById("dialog2")
	closeDialog(dialog2)
    
})