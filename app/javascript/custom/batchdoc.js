$(document).on("click","#addbatch",function(){
    if ($("#medproduct").val() != "" &&$("#batchnr").val() != ""){
        //here we created new tr , td ,button and input elements so we can append them to the preexistend table in the view
        let tr= $(document.createElement("tr"))
        let td1 = $(document.createElement("td"))
        let td2 = $(document.createElement("td"))
        let td3 = $(document.createElement("td"))
        let medinput = $(document.createElement("input"))
        let nrinput = $(document.createElement("input"))
        let delbtn = $(document.createElement("button"))
        //here we give each element its own atttributes: values , ids , names : names are important because they will be submitted
        delbtn.attr("type","button");delbtn.attr("id","deletebatch");delbtn.attr("class","btn btn-primary btn-sm");delbtn.html("Löschen")
        medinput.attr("type","text");medinput.attr("name","batchdoc[medproducts][]");medinput.attr("value",$("#medproduct").val());
        nrinput.attr("type","text");nrinput.attr("name","batchdoc[batchnrs][]");nrinput.attr("value",$("#batchnr").val());
        //here we begin to add the elements to the view 
        $("#batchlist tbody").append(tr)
        $(tr).append(td1)
        $(td1).after(td2)
        $(td2).after(td3)
        $(td1).append(medinput)
        $(td2).append(nrinput)
        $(td3).append(delbtn)
        //after clicking on the "+" button of the view the twi input field get empty
        $("#medproduct").val("")
        $("#batchnr").val("")
        // this is need for the "Zusammenfassung" because the human actions wont trigger the function in the summary.js
        $("#batchlist").trigger("change")
    }
        
})
// if you click "Löschen"-button from a created batchdata , it gets deleted
$(document).on("click","#deletebatch",function(){
    $(event.target).parent().parent().remove() 
     // this is need for the "Zusammenfassung" because the human actions wont trigger the function in the summary.jss
    $("#batchlist").trigger("change")
})