$(document).on("click", "#myRange" , function(){ 
  console.log(document.getElementById("myRange"))
  var slider = document.getElementById("myRange");
  var output = document.getElementById("Schmerz");
  output.innerHTML = slider.value;
  slider.oninput = function() {
    
    output.innerHTML = this.value;
    $("#Schmerz").val(output.innerHTML);
  }
  
  $(document).ready(function() {

    const $valueSpan = $('.valueSpan2');
    const $value = $('#myRange');
    $valueSpan.html($value.val());
    $value.on('input change', () => {
  
        $valueSpan.html($value.val());
    });
  });
})


