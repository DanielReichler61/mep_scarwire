//this function is for checking if our device is from ios , its important because the uploadbutton logic is different on ios
function iOS() {

    var iDevices = [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ];
  
    if (navigator.platform) {
      while (iDevices.length) {
        if (navigator.platform === iDevices.pop()){ return true; }
      }
    }
  
    return false;
  }
if (iOS() == false){
    let woundfilearray;
    let docfilearray;
    //if you click on the upload button, then we get the current files in the upload button and convert them into a filelist and save them in the variable woundfilearray
    $(document).on("click","#wound_images_",function(){  
        woundfilearray = Array.from(event.target.files)
        convertArrayToFileList(woundfilearray)
        //event.target.files = list.files
    })
      //if you click on the upload button, then we get the current files in the upload button and convert them into a filelist and save them in the variable docfilearray
    $(document).on("click","#documentation_images_",function(){ 
        docfilearray = Array.from(event.target.files)
        convertArrayToFileList(docfilearray)
        //event.target.files = list.files
    })
//this function is for converting arrays into filelist, this function isnt possible on ios BECAUSE IOS DOESNT SUPPORT THE OBJECT DATATRANSFER
    function convertArrayToFileList(array){
        let list = new DataTransfer;
        for(let i = 0; i<array.length ; i++){
            list.items.add(array[i])
        }
        return list.files
    }
//after selecting images the chagne event is triggered
    $(document).on("change","#wound_images_",function(){
        //here we remove all visible previews
        $("img").remove(".imgfilefield");
        //thats the upload button; target
        let target = document.getElementById("wound_images_")
        //here we add the old files(woundfilearray) to the new files by using arrays
        let resultarray = Array.from(target.files).concat(woundfilearray)
        // here we convert the newly created array to a Filelist
        target.files = convertArrayToFileList(resultarray)
        //each File will be read by a FileReader, that gets us the image data.After that we create for each File an image element and give them the image data with specific attributes(class,id,popover)
        for(let i = 0; i< target.files.length;i++){
            let file = target.files[i];
            let img = document.createElement("img");
            let reader = new FileReader();
            reader.onloadend = function(){
                img.src = reader.result;
                img.height = 100;
                img.width = 100;
                img.classList.add("imgfilefield");
                img.setAttribute("data-container","body")
                img.setAttribute("data-toggle","popover")
                img.setAttribute("data-trigger","focus")
                img.setAttribute("data-placement","top")
                img.setAttribute("tabindex","0")
                
            }
            reader.readAsDataURL(file);
            $("#wound_images_").after(img);
        }
    })
// if you click on a preview image then a popover will appear
    $(document).on("click","#new_images_wound .imgfilefield",function(){
        //this is needed to initialize a popovers
    $(event.target).popover({
        html: true,
        //content means its hggtml content and it will be fileld with a delete button
        content: function(){
            let btn = $(document.createElement("button"))
            btn.attr("type","button")
            btn.attr("id","filefield_delete")
            btn.html("Löschen")
            btn.attr("class","btn btn-danger")
            return btn
        }
    }).popover("show")
    })
    //equals logic as above
    $(document).on("click","#new_images_doc .imgfilefield",function(){
        $(event.target).popover({
            html: true,
            content: function(){
                let btn = $(document.createElement("button"))
                btn.attr("type","button")
                btn.attr("id","filefield_doc_delete")
                btn.html("Löschen")
                btn.attr("class","btn btn-danger")
                return btn
            }
        }).popover("show")
        })
        //that will be triggered after clicking on the delete button of the popover
    $(document).on("click","#filefield_delete",function(){
        //get all img elements in reverse order
        let reverse = $("#new_images_wound img").get().reverse()
        let index = 0;// the indexs will be used to find the right File in the Filelist
        for(index;$("#new_images_wound img").length;index++){  
            //each img element get iterrated. Now we need to find img that is in relation the popover    
            if (reverse[index] == $("img[aria-describedby='"+$(event.target).parent().parent().attr("id")+"']").get(0) ){
                break;
            }
        }
        //all files from the upload button into array
        let array = Array.from(document.getElementById("wound_images_").files)
        //delete the specific file with the index from above
        array.splice(index, 1)
        //converting array to filelist again , FILELISTS ARE READONLY
        document.getElementById("wound_images_").files = convertArrayToFileList(array)
        // needs to be done so there wont be duplicates
        woundfilearray = []
        //specific function must be trigggered as equal if a huamn chose new images
        $("#new_images_wound #wound_images_").trigger("change")
    })
    //same as above
    $(document).on("click","#filefield_doc_delete",function(){
        let reverse = $("#new_images_doc img").get().reverse()
        let index = 0;
        for(index;$("#new_images_doc img").length;index++){     
            if (reverse[index] == $("img[aria-describedby='"+$(event.target).parent().parent().attr("id")+"']").get(0) ){
                break;
            }
        }
        let array = Array.from(document.getElementById("documentation_images_").files)
        array.splice(index, 1) 
        document.getElementById("documentation_images_").files = convertArrayToFileList(array)
        docfilearray = []
        $("#new_images_doc #documentation_images_").trigger("change")
    })
    //logic for documentations begins here
    //same as at wound
    $(document).on("change","#documentation_images_",function(){
        $("img").remove(".imgfilefield");
        let target = document.getElementById("documentation_images_")
        let resultarray = Array.from(target.files).concat(docfilearray)
        target.files = convertArrayToFileList(resultarray)
        for(let i = 0; i< target.files.length;i++){
            let file = target.files[i];
            let img = document.createElement("img");
            let reader = new FileReader();
            reader.onloadend = function(){
                img.src = reader.result;
                img.height = 100;
                img.width = 100;
                img.classList.add("imgfilefield");
                img.setAttribute("data-container","body")
                img.setAttribute("data-toggle","popover")
                img.setAttribute("data-trigger","focus")
                img.setAttribute("data-placement","top")
                img.setAttribute("tabindex","0")

            }
            reader.readAsDataURL(file);
            $("#documentation_images_").after(img);
        }
    })

}else {
    //Thats the logic for IOS UPLAODButton
    //the logic here is that if you click on the upload button it will get hidden and a new one will be created and will appear, deleting and popver are as equals as before in the pc version
    $(document).on("change","#wound_images_",function(){
    $(event.target).attr("id"," ")
    let uploader = document.createElement("input")
    $(uploader).attr("type","file")
    $(uploader).attr("name","wound[images][]")
    $(uploader).attr("id","wound_images_")
    $(uploader).attr("multiple","multiple")
    $(uploader).attr("hidden","hidden")
    let span = document.createElement("span")
    $(span).attr("class","preview-images")
    $(span).attr("data-placement","top")
    $(span).attr("data-trigger","focus")
    $(span).attr("tabindex","0")
    let label = $("label[for='wound_images_']")
    $("#previews").append(span)
    $("label[for='wound_images_']").remove()
    for(let i = 0; i< event.target.files.length;i++){
        let file = event.target.files[i];
        let img = document.createElement("img");
        let reader = new FileReader();
        reader.onloadend = function(){
            img.src = reader.result;
            img.height = 100;
            img.width = 100;
            img.classList.add("imgfilefield");

        }
        reader.readAsDataURL(file);
        $(span).append(img);
        
    }
    $(event.target).after(uploader)
    $(uploader).after(label)
    $(span).append(event.target)
})
    $(document).on("change","#documentation_images_",function(){
        $(event.target).attr("id"," ")
        let uploader = document.createElement("input")
        $(uploader).attr("type","file")
        $(uploader).attr("name","documentation[images][]")
        $(uploader).attr("id","documentation_images_")
        $(uploader).attr("multiple","multiple")
        $(uploader).attr("hidden","hidden")
        let span = document.createElement("span")
        $(span).attr("class","preview-images")
        $(span).attr("data-placement","top")
        $(span).attr("data-trigger","focus")
        $(span).attr("tabindex","0")
        let label = $("label[for='documentation_images_']")
        $("#previews").append(span)
        $("label[for='documentation_images_']").remove()
        for(let i = 0; i< event.target.files.length;i++){
            let file = event.target.files[i];
            let img = document.createElement("img");
            let reader = new FileReader();
            reader.onloadend = function(){
                img.src = reader.result;
                img.height = 100;
                img.width = 100;
                img.classList.add("imgfilefield");
    
            }
            reader.readAsDataURL(file);
            $(span).append(img);
            
        }
        $(event.target).after(uploader)
        $(uploader).after(label)
        $(span).append(event.target)
    })
    $(document).on("click",".imgfilefield",function(){
        $(event.target).parent().popover({
            html: true,
            content: function(){
                let btn = $(document.createElement("button"))
                btn.attr("type","button")
                btn.attr("id","delete_previews")
                btn.html("Löschen")
                btn.attr("class","btn btn-danger")
                return btn
            }
        }).popover("show")
        })
}
$(document).on("click","#delete_previews",function(){
   $("span[aria-describedby='"+$(event.target).parent().parent().attr("id")+"']").remove()
})






//if previewimages are clicked  then a modal will be opened and we get the src from the clicked image so we can duplicate the image in the modal and show it in the modal

$(document).on("click",".popup_link",function(){
   let src = $(event.target).attr("src")
   let stringarray = src.split("/")
   let filename = stringarray[stringarray.length - 1]
   $("#modalundelete").prop("hidden",($(event.target).attr("class") == "popup_link thumbnail"))
   $("#modaldelete").prop("hidden",($(event.target).attr("class") == "popup_link thumbnail image-deleted"))
   //<input type="hidden" name="wound[images][]" id="wound_images_" value="a4d6cf094a728a5b-2020-05-29.jpg">
   $(".modal-title").html(filename)       
   $("#modalbody").html("<img id=\"modal_body_img\" src=\""+ src +"\">")
   $("#myModal").show();
   
})
// marks the image as green by adding a class to the preview picture after clicking delete
$(document).on("click","#modaldelete",function(){
    let src = $("#modal_body_img").attr("src")
    let stringarray = src.split("/")
    let filename = stringarray[stringarray.length - 1]
    $("input[value=\"" + filename + "\"]").remove();
    console.log($("img[src=\"" + src + "\"]"))
    $("img[src=\"" + src + "\"]").addClass("image-deleted")
    $("#myModal").hide();
})
$(document).on("click","#modalundelete",function(){
    let src = $("#modal_body_img").attr("src")
    let stringarray = src.split("/")
    let filename = stringarray[stringarray.length - 1]
    let hidden = $(document.createElement("input"))
    if (stringarray[4] == "Wundaufnahme"){
        console.log("Wundaufnahme")
        hidden.attr("type","hidden")
        hidden.attr("name","wound[images][]")
        hidden.attr("id","wound_images_")
        hidden.attr("value",filename)
    } else {
        hidden.attr("type","hidden")
        hidden.attr("name","documentation[images][]")
        hidden.attr("id","documentation_images_")
        hidden.attr("value",filename)
    }
    $("img[src=\"" + src + "\"] ").removeClass("image-deleted")  
    $("img[src=\"" + src + "\"].popup_link").after(hidden)
    $("#myModal").hide();
})  



$(document).on("click","#modalclose",function(){
    $("#myModal").hide();
})
$(document).on("click","#modaldismiss",function(){
    $("#myModal").hide();
})
// let the carousel stop the auto-cycling
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }