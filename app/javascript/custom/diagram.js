// the flag is needed two differentiate between the touch event and the click even, so they wont bother each other
let flag = false;
// this is logic for the pc 
$(document).on("click",".path-cl",function(){
    if (flag == false) {
        //clicked is to unclicked a marked area
        let clicked = false;
        //checks if the clicked area already got the class which triggers the red fill
        if($(event.target).attr("class")=="path-cl path-cl-selected"){
            clicked = true;
        }
        //gets the value of the clicked body part
        let value = $(event.target).attr("value");
        //select will include the element which is selected 
        let selected = document.getElementsByClassName("path-cl-selected")
        if(selected.length != 0){
        for (let item of selected){
            item.classList.remove("path-cl-selected") //here we remove the fill or the class thats fill, so they wont be extra red bodyparts
        }
        }
        if(clicked){
            $("#Localization").val("");
        }else{
            //here we add the marker to the clicked bodypart
            $(event.target).addClass("path-cl-selected");
            $("#Localization").val(value);
        }
        flag = false;
    }
    
})

//here give the hidden input field the value of the clicked body part 
$(document).on("change","#Localization",function(){
    let selected = document.getElementsByClassName("path-cl-selected")

    if(selected.length != 0){
       for (let item of selected){
           item.classList.remove("path-cl-selected") //here we remove the fill or the class thats fill
       }
    }
    let value = $("#Localization").val()
    $("path[value=\"" + value + "\"]").addClass("path-cl-selected")
})
//same principle with pc version only with touchevent
$(document).on("touchstart",".path-cl",function(){
    flag = true;
    let clicked = false;
    //checks if the clicked area already got the class which triggers the red fill
    if($(event.target).attr("class")=="path-cl path-cl-selected"){
        clicked = true;
    }
    //gets the value of the clicked body part
    let value = $(event.target).attr("value");
    //select will include the element which is selected 
    let selected = document.getElementsByClassName("path-cl-selected")
    if(selected.length != 0){
       for (let item of selected){
           item.classList.remove("path-cl-selected") //here we remove the fill or the class thats fill
       }
    }
    if(clicked){     
        $("#Localization").val("");
    }else{  
        $(event.target).addClass("path-cl-selected");
        $("#Localization").val(value);
    }
    
})




