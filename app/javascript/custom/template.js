var Trix = require("trix");

/* insert the button visual in the default toolbar */
addEventListener("trix-before-initialize", function(event) {
    var buttonHTML = '<button id="button-template" type="button" data-trix-attribute="template"><i class="fa fa-bolt"></i><i class="fa fa-file-text-o"></i></span></button>'

    event.target.toolbarElement.
    querySelector(".trix-button-group").
    insertAdjacentHTML("beforeend", buttonHTML)
})

function openDialog(template){
	if (template == null) return 
    template.classList.add('active')
    console.log(document.getElementById("overlay"))
	document.getElementById("overlay").classList.add('active')
}

function closeDialog(template){
    if (template == null) return 
    console.log(template)
    template.classList.remove('active')
    console.log(document.getElementById("overlay"))
    document.getElementById("overlay").classList.remove('active')
}

$(document).on("click", "#button-template" , function(){ 
    console.log(document.getElementById("template"))
	let template = document.getElementById("template")
	openDialog(template)
})

$(document).on("click", "#overlay" , function(){ 
	const templates=document.querySelectorAll('.template active')
	templates.forEach(template => {
		closeDialog(template)
	})
})


$(document).on("click", "#template-close-button" , function(){ 
    let template = document.getElementById("template")
		closeDialog(template)
})
$(document).on("click", "#save-template" , function(){ 
    console.log($(".auswahl-selected").html())
    console.log($(".auswahl-selected #hover_content").html())
    let value = $(".auswahl-selected #hover_content").html()
    $("trix-editor").val(value)
    let template = document.getElementById("template")
		closeDialog(template)

    

})




let flag = false;

// F12 on Browser then console 

$(document).on("click",".auswahl",function(){
    if (flag == false) {
        console.log("es wurde geklickt")
        let clicked = false;
        //checks if the clicked area already got the class which triggers the red fill
        if($(event.target).attr("class")=="auswahl auswahl-selected"){
            clicked = true;
        }
        
        //gets the value of the clicked body part
        let value = $(event.target).attr("value");
        //select will include the element which is selected 
        let selected = document.getElementsByClassName("auswahl-selected")
        if(selected.length != 0){
            for (let item of selected){
                console.log(item)
                item.classList.remove("auswahl-selected") //here we remove the fill or the class thats fill
            }
        }
        if(clicked){
            console.log("abgewählt click")
            $(".auswahl").val("");
        }else{
            console.log("ausgewählt click")
            $(event.target).addClass("auswahl-selected");
            $(".auswahl").val(value);
        }
        flag = false;
    }
    
})


$(document).on("change",".auswahl",function(){
    console.log("Kein Fokus")
    let selected = document.getElementsByClassName("auswahl-selected")
    if(selected.length != 0){
       for (let item of selected){
           console.log(item)
           item.classList.remove("auswahl-selected") //here we remove the fill or the class thats fill
       }
    }
    let value = $(".auswahl").val()
    $("path[value=\"" + value + "\"]").addClass("auswahl-selected")
})

$(document).on("touchstart",".auswahl",function(){
    flag = true;
    console.log("Es wurde getouched")
    let clicked = false;
    //checks if the clicked area already got the class which triggers the red fill
    if($(event.target).attr("class")=="auswahl auswahl-selected"){
        clicked = true;
    }
    //gets the value of the clicked body part
    let value = $(event.target).attr("value");
    //select will include the element which is selected 
    let selected = document.getElementsByClassName("auswahl-selected")
    if(selected.length != 0){
       for (let item of selected){
           console.log(item)
           item.classList.remove("auswahl-selected") //here we remove the fill or the class thats fill
       }
    }
    if(clicked){
        console.log("abgewählt touch")
        $(".auswahl").val("");
    }else{
        console.log("ausgewählt touch")
        $(event.target).addClass("auswahl-selected");
        $(".auswahl").val(value);
    }
    
})




